# Terraform basic knowledge

## Introduction

  Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

  Configuration files describe to Terraform the components needed to run a single application or your entire datacenter. Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied. 

## Use cases
* Herou App Setup  
* Resource Schedulers
* Multi-cloud deployment
    
  It's often attractive to spread infrastructure across multiple clouds to increase fault-tolerance. By using only a single region or cloud provider, fault tolerance is limited by the availability of that provider. Having a multi-cloud deployment allows for more graceful recovery of the loss of a region or entire provider.

  Realizing multi-cloud deployments can be very challenging as many existing tools for infrastructure management are cloud-specific. Terraform is cloud-agnostic and allows a single configuration to be used to manage multiple providers, and to even handle cross-cloud dependencies. This simplifies management and orchestration, helping operators build large-scale multi-cloud infrastructures.
   
## Getting started
* [Installation](https://learn.hashicorp.com/terraform/getting-started/install)

  Copy the terraform installation package address, in the command line :
  ``` 
  wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip  
  unzip terraform_version_linux_amd64.zip
  mkdir downloads    
  mv terraform downloads/
  ls -la
  vim ~/.profile
  ```
  Add at the end of the document : `export PATH="$PATH:~/downloads"`
    
  Then in the command line :    
  ```
  source ~/.profile
  terraform version
  ```
    
* [Build Infrastructure](https://learn.hashicorp.com/terraform/getting-started/build)
  * Configuration
    (view [configuration OpenStack Provider](https://www.terraform.io/docs/providers/openstack/index.html))

    The set of files used to describe infrastructure in Terraform is simply known as a Terraform configuration.
     
    Save the contents to a file named example.tf. Verify that there are no other `*.tf` files in your directory, since Terraform loads all of them.
     
    ```
    provider "aws" {
      access_key = "ACCESS_KEY_HERE"
      secret_key = "SECRET_KEY_HERE"
      region     = "us-east-1"
    }
    resource "aws_instance" "example" {
      ami           = "ami-2757f631"
      instance_type = "t2.micro"
    }
    ```
    
    Replace the `ACCESS_KEY_HERE` and `SECRET_KEY_HERE` with your AWS access key and secret key
    
    This is a complete configuration that Terraform is read to apply. The general structure should be intuitive and straightforward.
    
    The `provider` block is used to configure the names provider, in our case ""aws". A provider is responsible for creating and managing resources. Multiple provider blocks can exist if a Terraform configuration is composed of multiple providers, which is a common situation.
     
    The `resource` block defines a resource that exists within the infrastructure. A resource might be a physical component such as an EC2 instance, or it can be a logical resource such as a Heroku application.
    
    The resource block has two strings before opening the block: the resource type and the resource name. In our example, the resource type is "aws_instance" and the name is "example." The prefix of the type maps to the provider. In our case "aws_instance" automatically tells Terraform that it is managed by the "aws" provider.
     
    Within the resource block itself is configuration for that resource. 
  
  
  * Initialization
   
    The first command to run for a new configuration -- or after checking out an existing configutation from version control -- is `terraform init`, which initializes various lacal settings and data that will be used by subsequent commands.
     
  * Apply changes

    In the same directory as the example.tf file you created, run `terraform apply`. This output shows the execution plan, describing which actions Terraform will take in order to change real infrastructure to match the configuration.  
     
    Terraform also wrote some data into the `terraform.tfstate` file. This state file is extremely important; it keeps track of the IDs of created resources so that Terraform knows what it is managing. This file must be saved and distributed to anyone who might run Terraform. It is generally recommended to setup remote state when working with Terraform, to share the state automatically.
     
    You can inspect the current state using `terraform show`.
     
* [Change infrastructure](https://learn.hashicorp.com/terraform/getting-started/change)
  * Configuration
   
     Edit the `aws_instance.example` resource in the configuration and change it, For exemple modify the ami of our instance. You can also completely remove resources and Terraform will know to destroy the old one.
     
  * Apply changes

     After changing the configuration, run `terraform apply` again to see how Terraform will apply this change to the existing resources.
    
     The prefix `-/+` means that Terraform will destroy and recreate the resource, rather than updating it in-place. While some attributes can be updated in-place (which are shown with the ~ prefix), changing the AMI for an EC2 instance requires recreating it.
    
     As indicated by the execution plan, Terraform first destroyed the existing instance and then created a new one in its place. You can use `terraform show` again to see the new values associated with this instance.
    
* [Destroy infrastructure](https://learn.hashicorp.com/terraform/getting-started/destroy)
  
  Resources can be destroyed using the `terraform destroy` commande, which is similar to `terraform apply` but it behaves as if all of the resources have been removed from the configuration.
  
  The `-` prefix indicates that the instance will be destroyed. As with apply, Terraform shows its execution plan and waits for approval before making any changes. Answer `yes` to execute this plan and destroy the infrastructure.
    
* [Resource dependencies](https://learn.hashicorp.com/terraform/getting-started/dependencies)

  * Assigning an Elastic IP
    ```
    resource "aws_eip" "ip" {
       instance = "${aws_instance.example.id}"
    }
    ```
  * Apply changes
      
    Run `terraform apply` to see how Terraform plans to apply this change. 

  * Implicite and explicite dependencies
    
    By studying the resource attributes used in interpolation expressions, Terraform can automatically infer when one resource depends on another. In the example above, the expression `${aws_instance.example.id}` creates an implicit dependency on the `aws_instance` named `example`.
      
    Terraform uses this dependency information to determine the correct order in which to create the different resources. In the example above, Terraform knows that the `aws_instance` must be created before the `aws_eip`.
      
    Sometimes there are dependencies between resources that are not visible to Terraform. The `depends_on` argument is accepted by any resource and accepts a list of resources to create explicit dependencies for.

  * Non-dependent resources
  
    We can continue to build this configuration by adding another EC2 instance:
    ```
    resource "aws_instance" "another" {
      ami           = "ami-b374d5a5"
      instance_type = "t2.micro"
    }
    ```
    Because this new instance does not depend on any other resource, it can be created in parallel with the other resources.
     
* [Provision](https://learn.hashicorp.com/terraform/getting-started/provision)
 
  Let's see how to use provisioners to initialize instances when they're created.
   
  * Defining a provisioner
   
    To define a provisioner, modify the resource block defining the "example" EC2 instance to look like the following:
    ```
    resource "aws_instance" "example" {
      ami           = "ami-b374d5a5"
      instance_type = "t2.micro"
      provisioner "local-exec" {
        command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
      }
    }
    ```
    This adds a `provisioner` block within the `resource` block. Multiple `provisioner` blocks can be added to define multiple provisioning steps.
     
  * Running provisioners
   
    Provisioners are only run when a resource is created. They are not a replacement for configuration management and changing the software of an already-running server, and are instead just meant as a way to bootstrap a server.
     
    Make sure that your infrastructure is destroyed if it isn't already, then run `apply`. Terraform will output anything from provisioners to the console.
     
    we can verify everything worked by looking at the ip_address.txt file: `cat ip_addresse.txt`. It contains the IP, just as we asked!
      
  * Failed provisioners and tainted resources
   
    If a resource successfully creates but fails during provisioning, Terraform will error and mark the resource as "tainted".  A resource that is tainted has been physically created, but can't be considered safe to use since provisioning failed.
     
    When you generate your next execution plan, Terraform will not attempt to restart provisioning on the same resource because it isn't guaranteed to be safe. Instead, Terraform will remove any tainted resources and create new resources, attempting to provision them again after creation.
     
  * Destroy provisioners

    Provisioners can also be defined that run only during a destroy operation. These are useful for performing system cleanup, extracting data, etc.
    

* [Input variables](https://learn.hashicorp.com/terraform/getting-started/variables)
  
  * Defining variables 

    Extract our access key, secret key, and region into a few variables. Create another file `variables.tf` with the following contents.
    ```
    variable "access_key" {}
    variable "secret_key" {}
    variable "region" {
      default = "us-east-1"
    }
    ```
      
    If a default value is set, the variable is optional. Otherwise, the variable is required.  If you run terraform plan now, Terraform will prompt you for the values for unset string variables.
      
  * Using variables in configuration
      
    Next, replace the AWS provider configuration with the following: 
    ```
    provider "aws" {
      access_key = "${var.access_key}"
      secret_key = "${var.secret_key}"
      region     = "${var.region}"
    }
    ```
      
  * Assigning variables
      
    The following is the descending order of precedence in which variables are considered.
       
    * Command-line flags

      You can set variable directly on the comand-line with the `-var` flag. Any command in Terraform that inspects the configuration accepts this flag, such as `apply`, `plan`, and `refresh`:
      ```
      terraform apply \
      -var 'access_key=foo' \
      -var 'secret_key=bar'
      ```
      Setting variables this way will not save them, and they'll have to be input repeatedly as commands are executed.
      
    * From a file
      
      To persist variable values, create a file and assign variables within this file. Create a file named `terraform.tfvars` with the following contents:
      ```
      access_key = "foo"
      secret_key = "bar"
      ```
     
      For all files which match `terraform.tfvars` or `*.auto.tfvars` present in the current directory, Terraform automatically loads them to populate variables. If the file is named something else, you can use the `-var-file` flag directly to specify a file.
     
      You can use multiple -var-file arguments in a single command, with some checked in to version control and others not checked in. For example:
      ```
      terraform apply \
      -var-file="secret.tfvars" \
      -var-file="production.tfvars"
      ```
     
    * From environment variables

      Terraform will read environment variables in the form of `TF_VAR_name` to find the value for a variable.  For example, the `TF_VAR_access_key` variable can be set to set the `access_key` variable.
        
      **Environment variables can only populate string-type variables. List and map type variables must be populated via one of the other mechanisms.**
        
    * UI input

      If no value is assigned to a variable via any of these methods and the variable has a default key in its declaration, that value will be used for the variable.
        
  * Lists
        
    Lists are defined either explicitly or implicitly
    ```
    // implicitly by using brackets [...]
    variable "cidrs" { default = [] }
    // explicitly
    variable "cidrs" { type = "list" }
    ```
    
    You can specify lists in a terraform.tfvars file: `cidrs = [ "10.0.0.0/16", "10.1.0.0/16" ]`
    
  * Maps

    Maps are a way to create variables that are lookup tables. 
    ```
    variable "amis" {
      type = "map"
      default = {
      "us-east-1" = "ami-b374d5a5"
      "us-west-2" = "ami-4b32be2b"
      }
    }
    ```
    A variable can have a map type assigned explicitly, or it can be implicitly declared as a map by specifying a default value that is a map. The above demonstrates both.
    
    Then, replace the aws_instance with the following:
    ```
    resource "aws_instance" "example" {
      ami           = "${lookup(var.amis, var.region)}"
      instance_type = "t2.micro"
    }
    ```

    The `lookup` function does a dynamic lookup in a map for a key. The key is `var.region`, which specifies that the value of the region variables is the key.
     
    While we don't use it in our example, it is worth noting that you can also do a static lookup of a map directly with `${var.amis["us-east-1"]}`.
     
  * Assigning Maps

    We set defaults above, but maps can also be set using the -var and -var-file values. For example:`terraform apply -var 'amis={ us-east-1 = "foo", us-west-2 = "bar" }'`
      
    Here is an example of setting a map's keys from a file. Starting with these variable definitions:
    ```
    variable "region" {}
    variable "amis" {
      type = "map"
    }
    ```

    You can specify keys in a `terraform.tfvars` file:
    ```
    amis = {
      "us-east-1" = "ami-abc123"
      "us-west-2" = "ami-def456"
    }
    ```
     
    And access them via `lookup()`:
    ```
    output "ami" {
      value = "${lookup(var.amis, var.region)}"
    }
    ```
    
    Like so:`terraform apply -var region=us-west-2`
     
* [Output variables](https://learn.hashicorp.com/terraform/getting-started/outputs)
  * Defining Outputs
    
    Let's define an output to show us the public IP address of the elastic IP address that we create. Add this to any of your `*.tf` files:
    ```
    output "ip" {
      value = "${aws_eip.ip.public_ip}"
    }
    ```
      
  * Viewing Outputs
    
    Run` terraform apply` to populate the output.
       
    You can also query the outputs after apply-time using `terraform output` : `terraform output ip`
       
* [Modules](https://learn.hashicorp.com/terraform/getting-started/modules)
  
  Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. Modules are used to create reusable components, improve organization, and to treat pieces of infrastructure as a black box.
    
  * Using modules

    Create a configuration file with the following contents: 
    ```
    provider "aws" {
    access_key = "AWS ACCESS KEY"
    secret_key = "AWS SECRET KEY"
    region     = "us-east-1"
    }
    module "consul" {
      source = "hashicorp/consul/aws"
      version = "0.3.3"
      aws_region  = "us-east-1" # should match provider region
      num_servers = "3"
    }
    ```
      
    The `source` attribute is the only mandatory argument for modules. It tells Terraform where the module can be retrieved. Terraform automatically downloads and manages modules for you.
      
    The other attributes shown are inputs to our module. This module supports many additional inputs, but all are optional and have reasonable values for experimentation.
     
    After adding a new module to configuration, it is necessary to run (or re-run) `terraform init` to obtain and install the new module's source code
     
  * Apply changes

    With the Consul module (and its dependencies) installed, we can now apply these changes to create the resources described within.
     
     If you run `terraform apply`, you will see a large list of all of the resources encapsulated in the module. The output is similar to what we saw when using resources directly, but the resource names now have module paths prefixed to their names
  
  * Module outputs
     
    Just as the module instance had input arguments such as `num_servers` above, module can also produce output values, similar to resource attributes.
     
    One of the supported outputs is called `asg_name_servers`, and its value is the name of the auto-scaling group that was created to manage the Consul servers.
     
    Add the following to the end of the existing configuration file created above:
    ```
    output "consul_server_asg_name" {
      value = "${module.consul.asg_name_servers}"
    }
    ```
     
    The syntax for referencing module outputs is `${module.NAME.OUTPUT}`, where `NAME` is the module name given in the header of the module configuration block and `OUTPUT` is the name of the output to reference.
     
    If you run terraform apply again, Terraform will make no changes to infrastructure, but you'll now see the "consul_server_asg_name" output with the name of the created auto-scaling group
    
  * Destroy
     
    Just as with top-level resources, we can destroy the resources created by the Consul module to avoid ongoing costs: `terraform destroy`
     
* [Terraform remote](https://learn.hashicorp.com/terraform/getting-started/remote)

  Depending on the features you wish to use, Terraform has multiple remote backend options. You could use Consul for state storage, locking, and environments. This is a free and open source option. You can use S3 which only supports state storage, for a low cost and minimally featured solution.
   
  First, configure the backend in your configuration: 
  ```
  terraform {
    backend "consul" {
    address = "demo.consul.io"
    path    = "getting-started-RANDOMSTRING"
    lock    = false
    }
  }
  ```
  Please replace "RANDOMSTRING" with some random text. 
   
  The `backend` section configures the backend you want to use. After configuring a backend, run `terraform init` to setup Terraform. It should ask if you want to migrate your state to Consul. Say "yes" and Terraform will copy your state.
   
  Now, if you run terraform apply, Terraform should state that there are no changes.
   
  Terraform is now storing your state remotely in Consul. Remote state storage makes collaboration easier and keeps state and secret information off your local disk. Remote state is loaded only in memory when it is used.
   
  If you want to move back to local state, you can remove the backend configuration block from your configuration and run terraform init again. Terraform will once again ask if you want to migrate your state back to local.
   
---

## [Use terraform to deploy Openstack VMs](https://www.youtube.com/watch?v=IK6lRLDyzpY)

Terraform is a way to create you insfratructure as a code. Write you instastructure in a text file and you can use it to deploy your infrastructure. The file should be named `*.tf`

In the same folder, run 

`terraform init`

`terraform plan`

`terraform apply`

`terraform destroy`


## Reference
[Introduction to Terraform](https://www.terraform.io/intro/index.html)

[Use Terraform to deploy Openstack VMs](https://www.youtube.com/watch?v=IK6lRLDyzpY)
   
   
   
   