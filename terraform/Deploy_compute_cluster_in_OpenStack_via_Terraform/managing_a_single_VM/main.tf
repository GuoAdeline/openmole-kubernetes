resource "openstack_compute_keypair_v2" "my-cloud-key" {
           name = "my-key"
           public_key = "YOUR_PUBLIC_KEY"
}

resource "openstack_compute_instance_v2" "test" {
      name            = "test-vm"
      image_name      = "ubuntu-16.04-20160905"
      flavor_name     = "os.1"
      key_pair        = "${openstack_compute_keypair_v2.my-cloud-key.name}"
      security_groups = ["default"]
      network {
        name = "public"
      }
}
