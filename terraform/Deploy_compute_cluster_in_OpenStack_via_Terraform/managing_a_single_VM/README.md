# Managing a Single VM
## Obtaining credentials from OpenStack dashboard

You can download the environment file with the credentials from the OpenStack dashboard.

* Log in to the OpenStack dashboard, choose the project for which you want to download the OpenStack RC file, and click `API Access`.
* Click `Download OpenStack RC File` -> `OpenStack RC File (Identity API v3)` and save the file.

## Keypair

* Write in file `provider.tf` : 
  ```
  provider "openstack" {
      user_name = "YOUR_USER_NAME_HERE"
      password = "YOUR_PASSWORD_HERE"
      auth_url = "https://keystone.lal.in2p3.fr:5000/v3"
      region = "lal"
      user_domain_name = "stratuslab"
      project_domain_id = "6245e15fd3fc4faf9a5b8a2805f926eb"
  }
  ```
    
  You can get the information required above in your `OpenStack RC File`  

* Run `terraform init`, you will see a `.terraform` folder created.

* copy the public key
  ```
  cd ~/.ssh
  cat id_rsa.pub
  ```

* Write in file `main.tf`
  ```
  resource "openstack_compute_keypair_v2" "my-cloud-key" {
         name = "my-key"
         public_key = "YOUR_PUBLIC_KEY_HERE"
  }
  ```
 
* Run `terraform plan`
* Run `terraform apply` 
  
  Because we did not re-use our plan from the previous step, terraform will re-run the plan step first, and request your confirmation. Confirm that everything looks good and enter the value ‘yes’, and hit enter.

* In OpenStack, a flavor defines the compute, memory, and storage capacity of a virtual server, also known as an instance. (Only admin user can create a flavor)

* Adding an Instance
  * Write in file `main.tf`
    ```
    resource "openstack_compute_instance_v2" "test" {
      name            = "test-vm"
      image_name      = "ubuntu-16.04-20160905"
      flavor_name     = "os.1"
      key_pair        = "${openstack_compute_keypair_v2.my-cloud-key.name}"
      security_groups = ["default"]
      network {
        name = "public"
      }
    }
    ```

  * Run `source NAME_OF_OpenStack_RC_File_HERE`, for exemple : `source openmole-test-openrc.sh`, enter OpenStack Password for your project, in my case it's the password for openmole-test project (il me semble qu'il ne faut que lancer cette commande quand on redémarre l'ordi)

  * Run `terraform apply`
  * `Terraform show` and logging in
    * Run `Terraform show` to see the information of the machines 
    * Login with `ssh ubuntu@<ip address>`

## Reference
[Deploying a compute cluster in OpenStack via Terraform](https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html#managing-a-single-vm)