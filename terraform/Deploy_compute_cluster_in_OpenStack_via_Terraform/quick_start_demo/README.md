# Create OpenStack instances and deploy a kubernetes cluster 

## Prerequisite
* [Install Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

  If your are Linux user , run `terraform_installation.sh` file to install terraform.
  ```
  bash terraform_installation.sh
  ```
* Open `main.tf` file, change `YOUR_OPENSTACK_USERNAME` and `YOUR_OPENSTACK_PASSWORD` to your openstack username and password in block `variable "username"` and `variable "password"`
* Ensure that you have your Openstack credentials (`openmole-test-openrc.sh` in our example, view [here](/terraform/Deploy_compute_cluster_in_OpenStack_via_Terraform/managing_a_single_VM/#obtaining_credentials_from_openstack_dashboard) to see how to get it) loaded into environment variables. Likely via a command similar to:
  ```
  source openmole-test-openrc.sh 
  ```
* In this exemple we use image `ubuntu-16.04-20160905`. You can change it in the file `main.tf` at `variable "cluster_image"`

## Deployment
* In the `main.tf` file folder, run :

  ```
  terraform init
  terraform plan
  terraform apply
  ```

  After about 20 min, you will see the following information, which signifies that a kubernetes cluster is ready in OpenStack :
  ```
  Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

  Outputs:

  cluster-manager = $ ssh ubuntu@134.158.75.106
  kubernetes-node = $ ssh ubuntu@134.158.74.115 / $ ssh ubuntu@134.158.74.241
  ```

* We have deployed a kubernetes cluster with 2 nodes, we can access to kubernetes nodes by `ssh ubuntu@134.158.74.115` and `ssh ubuntu@134.158.74.241`. (Change IP according to your situation)

* `cluster-manager` is the machine to deploy kubernetes cluster. 

* We have also created a key pair named `mykey` using your public key. 
 
  **Attention** : this key pair can only be created once, if you are not running for the first time, please comment out these four lines in `main.tf` file:
  ```
  resource "openstack_compute_keypair_v2" "key-pair" {
    name = "${var.keypair}"
    public_key = "${file("~/.ssh/id_rsa.pub")}"
  }
  ```
  
## Destroy all
To destroy the cluster, run :
```
terraform destroy -var "username=YOUR_OPENSTACK_USERNAME" -var "password=YOUR_OPENSTACK_PASSWORD"
```
You will destroy kubernetes cluster and delete 3 instances OpenStack and the keypair.