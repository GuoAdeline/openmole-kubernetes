#!/usr/bin/env bash
wget https://releases.hashicorp.com/terraform/0.12.8/terraform_0.12.8_linux_amd64.zip
sudo apt install unzip
unzip terraform_0.12.8_linux_amd64.zip
sudo mv terraform /usr/local/bin/