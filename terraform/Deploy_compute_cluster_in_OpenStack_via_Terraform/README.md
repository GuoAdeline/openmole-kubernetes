# Deploying a compute cluster in OpenStack via Terraform

This project uses the OpenStack provider for Terraform. Other Cloud providers are available, but their invocation will be different from that which is described here.

## [Managing a Single VM](terraform/Deploy_compute_cluster_in_OpenStack_via_Terraform/managing_a _single_VM)

Creating a single machine in OpenStack

## [Managing a cluster of VMs](terraform/Deploy_compute_cluster_in_OpenStack_via_Terraform/managing_a_cluster_of_VMs)

Creating a cluster of machines in OpenStack

## [Quick start](terraform/Deploy_compute_cluster_in_OpenStack_via_Terraform/quick_start)

Creating a cluster of machines in OpenStack and deploying Kubernetes

## Tearing Everything Down
  * Simple delete your `main.tf` file (or rename it without the `.tf` extension)

  * Run `terraform apply`. Terraform will see that the resource is no longer part of your code and remove it.

## Reference
[Deploying a compute cluster in OpenStack via Terraform](https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html)