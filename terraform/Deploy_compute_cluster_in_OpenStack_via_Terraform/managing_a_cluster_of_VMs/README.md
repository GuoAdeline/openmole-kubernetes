# Managing a cluster of VMs

## Obtaining credentials from OpenStack dashboard

You can download the environment file with the credentials from the OpenStack dashboard.

* Log in to the OpenStack dashboard, choose the project for which you want to download the OpenStack RC file, and click `API Access`.
* Click `Download OpenStack RC File` -> `OpenStack RC File (Identity API v3)` and save the file.

## Confguration

Start by setting up the new nodes and lauching an entire cluster  
    
* We will launch a VM to act as our HTCondor central manager
* We will launch a VM to act as an NFS server for our “cluster”
* We will launch two execution nodes to process jobs

## cloud-init
  * These VMs that we have launched currently are all absolutely identical, except for the host name. There is no specialisation. They all do nothing. We will fix that and make these machines have individual roles by booting them with metadata attached to each instance.
  * `Cloud-init` is used for this process, it allows for injecting files and executing commands as part of the boot process.
  * Edit `main.tf` and `provider.tf`. In `provider.tf`, enter your `user_name` and `password` for OpenStack.

## Creating cluster
* Run `source NAME_OF_OpenStack_RC_File_HERE`, for exemple : `source openmole-test-openrc.sh`
* Run `terraform apply`

  We now have a running cluster! 

## Infrastructure Graph

Terraform has the ability to produce a graph showing the relationship of resources in your infrastructure. We will produce a graphic for our current terraform plan:
* run
  ```
  sudo apt install graphviz
  terraform graph | dot -Tpng > graph.png
  ```
  we get a `graph.png` file to describe the structure of the cluster :

![](./figs/graph.png)

## Reference
[Deploying a compute cluster in OpenStack via Terraform](https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html#managing-a-cluster-of-vms)