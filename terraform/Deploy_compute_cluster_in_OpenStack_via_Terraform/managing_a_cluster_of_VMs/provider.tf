provider "openstack" {
	auth_url = "https://keystone.lal.in2p3.fr:5000/v3"
	user_name = "YOUR_USER_NAME_HERE"
	password = "YOUR_PASSWORD_HERE"
	region = "lal"
	insecure = "true"
	user_domain_name = "stratuslab"
        project_domain_id = "6245e15fd3fc4faf9a5b8a2805f926eb"
}

