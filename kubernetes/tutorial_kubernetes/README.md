# Kubernetes tutorial

* [Introduction](#introduction)
* [1. Create a Cluster](#1-create-a-cluster)
  * [1.1. Kubernetes Cluster](#11-kubernetes-cluster)
  * [1.2. Tutorial : Creating a Cluster : deploy a local development Kubernetes cluster using minikube](#12-tutorial-creating-a-cluster-deploy-a-local-development-kubernetes-cluster-using-minikube)
* [2. Deploy an App](#2-deploy-an-app)
  * [2.1. Using kubectl to Create a Deployment](#21-using-kubectl-to-create-a-deployment)
  * [2.2 Tutorial : Deploying an App](#22-tutorial-deploying-an-app)
* [3. Explore you App](#3-explore-you-app)
  * [3.1. Viewing Pods and Nodes](#31-viewing-pods-and-nodes)
  * [3.2 Tutorial : Exploring your App](#32-tutorial-exploring-your-app)
* [4. Expose your App publicly](#4-expose-your-app-publicly)
  * [4.1. Using a service to expose your app](#41-using-a-service-to-expose-your-app)
  * [4.2. Tutorial : Exposing your App](#42-tutorial-exposing-your-app)
* [5. Scale your App](#5-scale-your-app)
  * [5.1. Running multiple instances of your App](#51-running-multiple-instances-of-your-app)
  * [5.2. Tutorial : Scaling your App](#52-tutorial-scaling-your-app)
* [6. Update your App](#6-update-your-app)
  * [6.1. Performing a Rolling Update](#61-performing-a-rolling-update)
  * [6.2. Tutorial : Updating your App](#62-tutorial-updating-your-app)
* [Reference](#reference)

## Introduction
Kubernetes is an open-source container orchestration system for automating application deployment, scaling, and management.

It's designed to provide "automated deployment, scaling, and platform for running application containers across clusters of hosts"

* Kubernetes Objects:
 * Pod : basic scheduling unit in Kubernetes. A higher level of abstraction can be added to the containerized component through this abstract class. A pod typically contains one or more containers so that they are always on the host and can share resources. Each pod is assigned a unique IP address, allowing the application to use the same port to avoid conflicts.
 * Services ： A set of pods that work together, a layer in a multi-tier architecture application. The pod group that constitutes the service is defined by a tag selector.
 * Volumes : Filesystems in the Kubernetes container provide ephemeral storage. This means that a restart of the container will wipe out any data on such containers, and therefore, this form of storage is quite limiting in anything but trivial application. A Kubernetes Volume provides persistence storage that exist for the lifetime of the pod itself.
 * Namespaces :  It is another very important concept in the Kubernetes system. By "allocating" objects inside the system to different Namespaces, different items, groups or user groups logically grouped are formed, which facilitates sharing of the entire cluster by different groups. Resources can also be managed separately.
After the Kubernetes cluster is started, it will create a Namespace named "default", which can be viewed by Kubectl.
Using Namespace to organize various objects of Kubernetes, you can group users, that is, "multi-tenancy" management. Separate resource quota setting and management can also be performed for different tenants, making the resource configuration of the entire cluster very flexible and convenient.
 
 * Labels and selectors : Label is a core concept in the Kubernetes system. Labels are attached to various objects in the form of key/value key-value pairs, such as Pod, Service, RC, Node, and so on. Label defines the identifiable properties of these objects for managing and selecting them. Labels can be attached to an object at creation time or managed through an API after the object is created.
After you have defined a Label for an object, other objects can use the Label Selector to define which objects they work with.

    There are currently two types of Label Selectors: Equality-based and Set-based, which can be combined to select multiple Labels.
    
    The equation-based Label Selector uses an expression of the equality class to make the selection:
    * name = redis-slave: Select all objects that contain key="name" in the Label and value="redis-slave";
    * env != production: Select all objects that include key="env" in the Label and whose value is not equal to "production".
    
    The collection-based Label Selector uses the expression of the collection operation to make the selection:
    * name in (redis-master, redis-slave): Select all objects that contain key="name" in the Label and value="redis-master" or "redis-slave";
    * name not in (php-frontend): Select all objects that contain key="name" in the Label and whose value is not equal to "php-frontend".


## 1. Create a Cluster
### 1.1. Kubernetes Cluster
* Kubernetes Clusters : Kubernetes coordinates a highly available cluster of computers that are connected to work as a single unit. The abstractions in Kubernetes allow you to deploy containerized application. To make use of this new model of deployment, applications need to be packaged in a way that decouples them from individual hosts: they need to be containerized. Containerized applications are more flexible and available than in past deployment models, where applications were installed directly onto specific machines as packages deeply integrated into the host. Kubernetes automates the distribution and scheduleing of application containers across a cluster on a more efficient way.
* A Kubernetes cluster consists of two types of resources:
 * The **Master** coordinates the cluster, it coordinates all activities in the cluster, such as scheduling applications, maintaining
 * **Nodes** are the workers that run applications. It's a VM or a physical computer that serves as a worker machine in a Kubernetes cluster. Each node has a Kubelet, which is an agent for managing the node and communicating with the Kubernetes master. A Kubernetes cluster that handles production traffic should have a minimum of three nodes. 
 
### 1.2. **Tutorial** : Creating a Cluster : deploy a local development Kubernetes cluster using minikube
 *  Cluster up and running
   * Check the minikube is installed : `minikube version`
   * Start the cluster :  `minikube start`
 * Cluster version
   (to interact with Kubernetes, we use the command line interface, kubectl): `kubectl version` (we can see both the version of the client and as well as the server. The client version is the kubectl version; the server version is the Kubernetes version installed on the master.) 
   
* Cluster details : `kubectl cluster-info`
* To view the nodes in the cluster: `kubectl get nodes`

## 2. Deploy an App
### 2.1. Using kubectl to Create a Deployment
* **Kubernetes Deployments** :  Once having running Kubernetes cluster, you can deploy your containerized application on top of it. To do so, you create a Kubernetes Deployment configuration. The Deployment instructs Kubernetes how to create and update instances of your application. Once you've created a Deployment, the Kuebernetes master schedules mentioned application instances onto individual Nodes in the cluster. Once the application instances are created, a Kubernetes Deployment Controller continuously monitors those instances. If the Node hosting an instance goes down or is deleted, the Deployment Controller replaces the instance with an instance on another Node in the cluster. This provides a self-healing mechanism to adress machine failure maintenance.
* **Deploying your first app on Kubernetes** : When you create a Deployment, you'll need to specify the container image for your application and the number of replicas that you want to run. For our first Deployment, we'll use a Node.js application packaged in a Docker container. 

### 2.2 **Tutorial** : Deploying an App
* kubectl basics
 
   The common format of a Kubectl commande is : **`kubectl action resource`**. 
 
   Ckeck that kubectl is configured to talk to your cluster, by running the command: `kubectl version`. You can see both the client version and server version.
 
   To view the nodes in the cluster, run the command: `kubectl get nodes`
   
* Deploy our app
 
	Run our first app on Kubernetes with the `kubectl run` command. The `run` command create a new deployment. We need to provide the deployment name and app image location (including the full repository url for images hosted outside Docker hub). We want to run the app on a specific port so we add the `--port` parameter : `kubectl run kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080`
   
   We've just deployed the first application by creating a deployment. This performed a few things:
   
    * searched for a suitable node where an instance of the application could be run
    * scheduled the application to run on the Node
    * configured the cluster to reschedule the instance in a new Node when needed

	To list the deployments, use the command : `kubectl get deployments`. We can see 1 deployment running 1 instance of the app.
    
    The instance is running inside a single instance of your app.
    
* View our app
 
   Pods that are running inside Kubernetes are running on a private, isolated network. When we use `Kubectl`, we're interacting through an API endpoint to communicate with our application.
   
   The `kubectl` command can creste a proxy that will forward communications into the cluster-wide, private network. The proxy can be terminated by pressing control-C and won't show any output while its running.
   
   Open a second terminal window to run the proxy : `kubectl proxy`  
   
   We now have a connection between our host (the online terminal) and the Kubernetes cluster. The proxy enables direct access to the API from these terminals. 
   
   For exemple, query the version derectly through the API using the `curl`command : ` curl http://localhost:8001/version`
   
   The API server will automatically create an endpoint for each pod, based on the pod name, that is also accessible through the proxy.
   
   First we need to get the Pod name, and we'll store in the environnement variable POD_NAME : 
   ```
   export POD_NAME=$(kubectl get pods -o go-template --template '{{range.items}}{{.metadata.name}}{{"\n"}}{{end}}') 
   
   echo Name of the Pod: $POD_NAME
   ```
   
   Now we can make an HTTP request to the application running in that pod :
   
   `curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/`
   
   The url is the route to the API of the Pod.
   
## 3. Explore you App
### 3.1. Viewing Pods and Nodes
* Kubernetes Pods 
  
  When you create a Deployment in Module 2, Kubernetes created a Pod to host your application instances. A Pod is a Kubernetes abstraction that represents a group of one or more application containers (such as Docker), and some shared resources for those containers, including :
  * Shared storage, as Volumes
  * Networking, as a unique cluster IP address
  * Information about how to run each container, such as the container image version or specific ports to use

  A Pod models an application-specific 'logical host' and can contain different application containers which are relatively tightly coupled. For exemple, a Pod might include both the container with your Node.js app as well as a different container that feeds the data to be published by the Node.js webserver. The containers in a Pod share an IP Address and port space, are always co-located and co-scheduled, and run in a shared context on the same Node.
  
  Pods are the atomic unit on the Kubernetes platform. When we create a Deployment on Kubernetes, that Deployment creates Pods with containers inside them (as opposed to creating containers directly). Each Pod is tied to the Node where it is scheduled, and remains there until termination (according to restart policy) or deletion. In case of a Node failure, ifentical Pods are scheduled on other available Node in the cluster.
  
* Nodes 

 A Pod always runs on a Node. A node is a worker machine in Kubernetes and may be either a virtual or a physical machine, depending on the cluster. Each node is managed by the Master. A node can have multiple pods, and the kubernetes master automatically handles scheduling the pods across the Nodes in the cluster. The Master's automatic scheduling takes into account the available resources on each Node.
 
 Every Kubernetes Node runs at least:
  * Kubelet, a process responsible for communication betwwen the Kubernetes Master and the Node; it manages the Pods and the containers running on a machine.
  * A container runtime (like Docker, rkt) responsible for pulling the container image from a registry, unpacking the container, and running the application.

* Troubleshooting with kubectl
  Use `kubectl`command-line interface to get information about deployed applications and their environments. The most common operations can be done with the following kubectl commands :
	* `Kubectl get` : list resources
	* `kubectl describe` : show detailed information about a resource
	* `kubectl logs` : print the logs from a container in a pod
	* `kubectl exec` : execute a command on a container in a pod

### 3.2 **Tutorial** : Exploring your App
* Check application configuration
  
  Use `kubectl get pods` to look for existing pods.
  If no pods are running, list the pods again.
  
  Next, to view what containers are inside that Pod and what images are used to build those containers we run the `describe pods` command : `kubectl describe pods` (we see here details about the pod's container: IP adderess, the ports used and a list of events related to the lifecycle of the pod)
  
* Show the app in the terminal
  
  Recall that Pods are running in an isolated, private network, so we need to proxy access to them so we can debug and interact with them. To do this, we'll use the `kubectl proxy` command to run a proxy in a second terminal window.
  
  Now again, we'll get the Pod name and query that pod directly through the proxy. To get the Pod name and store it in the POD_NAME environment variable : 
   ```
   export POD_NAME=$(kubectl get pods -o go-template --template '{{range.items}}{{.metadata.name}}{{"\n"}}{{end}}')    
   echo Name of the Pod: $POD_NAME
   ```
   
   To see the output of our application, run a `curl` request : 
   
   `curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/`
   
* View the container logs

  Anything that the application would normally send to `STDOUT` becomes ligs for the container within the Pod. We can retrive these logs using the command : `kubectl logs $POD_NAME`  

* Executing command on the container

  We can execute commands directly on the container once the Pod is up and running. For this, we use the `exec` command and use the name of the Pod as a parameter.
  
  To list the environment variable : `kubectl exec $POD_NAME env`
  
  The name of the container itself can be omitted since we only have a single container in the Pod.
  
  Next, let's start a bash session in the Pod's container : `kubectl exec -ti $POD_NAME bash`. We have now an open console on the container where we run our Node.JS application. The source code of the app is in the server.js file : `cat server.js`
  
  You can check that the application is up by running a curl command : `curl localhost:8080` (NOTE: here we used localhost because we executed the command inside the NodeJS container)
  
  To close your container connection, type : `exit`
  
## 4. Expose your App publicly
### 4.1. Using a service to expose your app

* Overview of Kubernetes services

  Kubernetes Pods are mortal. Pods in fact have a lifcycle. When a worker node dies, the Pods running on the Node are also lost. A ReplicaSet might then dynamically drive the cluster back to desired state via creation of new Pods to keep your application running. As another example, consider an image-processing backend with 3 replicas. Those replicas are exchangeable; the front-end system should not care about backend replicas or even if a Pod is lost and recreated. That said, each Pod in a Kubernetes cluster has a unique IP address, even Pods on the same Node, so there needs to be a way of automatically reconciling changes among Pods so that your applications continue to function. 
  
  A Service in Kubernetes is an abstraction which defines a logical set of Pods and a policy by which to access them. Services enable a loose coupling between dependent Pods. A Service is defined using YAML (preferred) or JSON, like all Kubernetes objects. The set of Pods targeted by a Service is usually determined by a LabelSelector.
  
  Although each Pod has a unique IP address, those IPs are not exposed outside the cluster without a Service. Services allow your applications to receive traffic. Services can be exposed in different ways by specifying a **type** in the ServiceSpec:
  * **ClusterIP** (default) - Exposes the Service on an internal IP in the cluster. This type makes the Service only reachable from within the cluster.
  * **NodePort** - Exposes the Service on the same port of each selected Node in the cluster using NAT. Makes a Service accessible from outside the cluster using `<NodeIP>:<NodePort>`. Superset of ClusterIP.
  * **LoadBalancer** - Creates an external load balancer in the current cloud (if supported) and assigns a fixed, external IP to the Service. Superset of NodePort.
  * **ExternalName** - Exposes the Service using an arbitrary name (specified by `externalName` in the spec) by returning a CNAME record with the name. No proxy is used. This type requires v1.7 or higher of `kube-dns`.

  A Service created without `selector` will also not create the corresponding Endpoints object. This allows users to manually map a Service to specific endpoints. Another possibility why there may be no selector is you are strictly using `type:ExternalName`

* Service and Labels

  A Service routes traffic across a set of Pods. Services are the abstraction that allow pods to die and replicate in Kubernetes without impacting your application. Discovery and routing among dependent Pod (such as the frontend and backend components in an application) is handled by Kubernetes Services.
  
  Services match a set of Pods using **labels and selectors**, a grouping primitive that allows logical operation on objects in Kubernetes. Labels are key/value pairs attached to objects and can be used in any number of ways:
  * Designate objects for development, test, and production
  * Embed version tags 
  * Classify an object using tags

  Labels can be attached to objects at creation time or later on. They can bef modified at any time.
  
### 4.2. **Tutorial** : Exposing your App
(Learn how to expose Kubernetes applications outside the cluster using the kubectl expose command and how to view and apply labels to objects with the kubectl label command.)

* Create a new service
  
  Look for existing Pods: `kubectl get pods`
  
  List the current Services from the cluster: `kubectl get services`. We have a Service called kubernetes that is created by default when minikube starts the cluster. To create a new service and expose it to external traffic we’ll use the expose command with NodePort as parameter (minikube does not support the LoadBalancer option yet).
  
  `kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080`
  
  Run again the command : `kubectl get services`. We have now a running Service called kubernetes-bootcamp. Here we see that the Service received a unique cluster-IP, an internal port and an external-IP (the IP of the Node).
  
  To find out what port was opened externally (by the NodePort option) we’ll run the command: `kubectl describe services/kubernetes-bootcamp`
  
  Create an environment variable called NODE_PORT that has the value of the Node port assigned :  
  ```
  export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
  echo NODE_PORT=$NODE_PORT
  ```
  
  Now we can test that the app is exposed outside of the cluster using curl, the IP of the Node and the externally exposed port: `curl $(minikube ip):$NODE_PORT`
  
* Using labels
  
  The Deployment created automatically a label for our Pod. With `describe deployment` command you can see the name of the label: `kubectl describe deployment`
  
  Use this label to query our list of Pods. We’ll use the `kubectl get pods` command with -l as a parameter, followed by the label values: `kubectl get pods -l run=kubernetes-bootcamp`
  
  You can do the same to list the existing services: `kubectl get services -l run=kubernetes-bootcamp`
  
  Get the name of the Pod and store it in the POD_NAME environment variable: 
  ```
  export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
  echo Name of the Pod: $POD_NAME
  ```
  
  To apply a new label we use the label command followed by the object type, object name and the new label: `kubectl label pod $POD_NAME app=v1`. This will apply a new label to our Pod (we pinned the application version to the Pod), and we can check it with the describe pod command: `kubectl describe pods $POD_NAME`. We see here that the label is attached now to our Pod. And we can query now the list of pods using the new label: `kubectl get pods -l app=v1` and we see the Pod.
  
* Deleting a service
  
  To delete Services you can use the delete service command. Labels can be used also here: `kubectl delete service -l run=kubernetes-bootcamp`
  
  Confirm that the service is gone: `kubectl get services`. This confirms that our Service was removed. To confirm that route is not exposed anymore you can curl the previously exposed IP and port: `curl $(minikube ip):$NODE_PORT`. This proves that the app is not reachable anymore from outside of the cluster. You can confirm that the app is still running with a curl inside the pod: `kubectl exec -ti $POD_NAME curl localhost:8080`. We see here that the application is up.
  
## 5. Scale your App
### 5.1. Running multiple instances of your App

* Scaling an application

  In the previous, the deployment created only one Pod for running our application. When traffic increases. we will need to scale the application to keep up with the user demand.
  
  **Scaling** is accomplished by changing the number of replicas in a Deployment.
  
  PS: You can create from the start a Deployment with multiple instances using the --replicas parameter for the kubectl run command.
  
  Scaling out a Deployment will ensure new Pods are created and scheduled to Nodes with available resources. Scaling will increase the number of Pods to the new desired state. Kubernetes also supports autoscaling of Pods, but it is outside of the scope of this tutorial. Scaling to zero is also possible, and it will terminate all Pods of the specified Deployment.

  Running multiple instances of an application will require a way to distribute the traffic to all of them. Services have an integrated load-balancer that will distribute network traffic to all Pods of an exposed Deployment. Services will monitor continuously the running Pods using endpoints, to ensure the traffic is sent only to available Pods.

  Scaling is accomplished by changing the number of replicas in a Deployment.

   Once you have multiple instances of an Application running, you would be able to do Rolling updates without downtime. 
  
### 5.2. **Tutorial** : Scaling your App

* Scaling a deployment

  To list the deployments, use the command: `kubectl get deployments` 

  The DESIRED state is showing the configured number of replicas

  The CURRENT state show how many replicas are running now

  The UP-TO-DATE is the number of replicas that were updated to match the desired (configured) state

  The AVAILABLE state shows how many replicas are actually AVAILABLE to the users.
  
  Next, let’s scale the Deployment to 4 replicas. We’ll use the `kubectl scale` command, followed by the deployment type, name and desired number of instances: `kubectl scale deployments/kubernetes-bootcamp --replicas=4`
  
   To list your Deployments once again, use `get deployments` : `kubectl get deployment`
   
   The change was applied, and we have 4 instances of the application available. Next, let’s check if the number of Pods changed: `kubectl get pods -o wide`

  There are 4 Pods now, with different IP addresses. The change was registered in the Deployment events log. To check that, use the describe command: `kubectl describe deployment/kubernetes-bootcamp`. You can also view in the output of this command that there are 4 replicas now.
  
* Load Balancing

  Let’s check that the Service is load-balancing the traffic. To find out the exposed IP and Port we can use the describe service as we learned in the previously Module: `kubectl describe services/kubernetes-bootcamp` 

  Create an environment variable called NODE_PORT that has a value as the Node port:
  ```
  export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
  echo NODE_PORT=$NODE_PORT
  ```
  
  Next, we’ll do a `curl` to the exposed IP and port. Execute the command multiple times:`curl $(minikube ip):$NODE_PORT`. We hit a different Pod with every request. This demonstrates that the load-balancing is working.
  
* Scale Down 

  To scale down the Service to 2 replicas, run again the `scale` command: `kubectl scale deployments/kubernetes-bootcamp --replicas=2` 

  List the Deployments to check if the change was applied with the `get deployments` command: `kubectl get deployments`.
  
  The number of replicas decreased to 2. List the number of Pods, with `get pods` : `kubectl get pods -o wide`. This confirms that 2 Pods were terminated.
  
## 6. Update your App
### 6.1. Performing a Rolling Update

* Updating an application

  Users expect applications to be available all the time and developers are expected to deploy new versions of them several times a day. In Kubernetes this is done with **rolling updates**. Rolling updates allow Deployments' update to take place with zero downtime by incrementally updating Pods instances with new ones. The new Pods will be scheduled on Nodes with available resources.
  
  In the previous module we scaled our application to run multiple instances. This is a requirement for performing updates without affecting application availability. By default, the maximum number of Pods that can be unavailable during the update and the maximum number of new Pods that can be created, is one. Both options can be configured to either numbers or percentages (of Pods). In Kubernetes, updates are versioned and any Deploument update can be reverted to previous (stable) version.
  
  Similar to application Scaling, if a Deployment is exposed publicly, the Service will load-balance the traffic only to available Pods during the update. An available Pod is an instance that is available to the users of the application.

  Rolling updates allow the following actions:

    * Promote an application from one environment to another (via container image updates)
    * Rollback to previous versions
    * Continuous Integration and Continuous Delivery of applications with zero downtime

### 6.2. **Tutorial** : Updating your App

(The goal of this scenario is to update a deployed application with kubectl set image and to rollback with the rollout undo command.)

* Update the version of the app

  To list your deployments use the get deployments command: `kubectl get deployments`

  To list the running Pods use the get pods command: `kubectl get pods`

  To view the current image version of the app, run a describe command against the Pods (look at the Image field): `kubectl describe pods`

  To update the image of the application to version 2, use the set image command, followed by the deployment name and the new image version: `kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=jocatalin/kubernetes-bootcamp:v2`

  The command notified the Deployment to use a different image for your app and initiated a rolling update. Check the status of the new Pods, and view the old one terminating with the get pods command: `kubectl get pods`.
  
* Verify an update

  First, let’s check that the App is running. To find out the exposed IP and Port we can use describe service: `kubectl describe services/kubernetes-bootcamp`

  Create an environment variable called NODE_PORT that has the value of the Node port assigned:  
  ```
  export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
  echo NODE_PORT=$NODE_PORT
  ```

  Next, we’ll do a `curl` to the the exposed IP and port: `curl $(minikube ip):$NODE_PORT`

  We hit a different Pod with every request and we see that all Pods are running the latest version (v2).

  The update can be confirmed also by running a rollout status command: `kubectl rollout status deployments/kubernetes-bootcamp`

  To view the current image version of the app, run a describe command against the Pods: `kubectl describe pods`

  We run now version 2 of the app (look at the Image field)
  
* Rollback an update

  Let’s perform another update, and deploy image tagged as v10 : `kubectl set image deployments/kubernetes-bootcamp kubernetes-bootcamp=gcr.io/google-samples/kubernetes-bootcamp:v10`

  Use get deployments to see the status of the deployment: `kubectl get deployments`

  And something is wrong… We do not have the desired number of Pods available. List the Pods again: `kubectl get pods`

  A describe command on the Pods should give more insights: `kubectl describe pods`

  There is no image called `v10` in the repository. Let’s roll back to our previously working version. We’ll use the rollout undo command: `kubectl rollout undo deployments/kubernetes-bootcamp`

  The rollout command reverted the deployment to the previous known state (v2 of the image). Updates are versioned and you can revert to any previously know state of a Deployment. List again the Pods: `kubectl get pods`

  Four Pods are running. Check again the image deployed on the them: `kubectl describe pods`

  We see that the deployment is using a stable version of the app (v2). The Rollback was successful.
  
# Reference
[Kubernetes Tutorial](https://kubernetes.io/docs/tutorials/)