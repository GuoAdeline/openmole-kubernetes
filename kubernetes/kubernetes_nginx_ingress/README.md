# Nginx Ingress
## Deploy `Ingress Nginx` in kubernetes cluster :
* ingress Nginx deployment
  ```
  kubectl apply -f ingress_nginx.yaml
  ```
* add external IP to ingress:
  ```
  kubectl patch svc <svc-name> -n <namespace> -p '{"spec": {"type": "LoadBalancer", "externalIPs":["<ADRESSE IP>"]}}'
  ```
  for exemple : 
  ```
  kubectl patch svc ingress-nginx -n ingress-nginx -p '{"spec": {"type": "LoadBalancer", "externalIPs":["134.158.75.115"]}}'
  ```
* Verify installation
    
  To check if the ingress controller pods have started, run the following command:
  ```
  kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx --watch
  ```
  Once the operator pods are running, you can cancel the above command by typing Ctrl+C. Now, you are ready to create your first ingress.
  
* Detect installed version
  ```
  POD_NAMESPACE=ingress-nginx
  POD_NAME=$(kubectl get pods -n $POD_NAMESPACE -l app.kubernetes.io/name=ingress-nginx -o jsonpath='{.items[0].metadata.name}')
  kubectl exec -it $POD_NAME -n $POD_NAMESPACE -- /nginx-ingress-controller --version
  ```
* Check the Nginx Configuration
  ```
  kubectl exec -it -n ingress-nginx nginx-ingress-controller-XXXXXXXXXX cat /etc/nginx/nginx.conf
  ```

## Basic Auth with Kubernetes Ingress   
* Create user and password : (openmoleuser / openmoleuser)
  ```
  htpasswd -c ./auth openmoleuser
  ```
* Create a secret :
  ```
  kubectl create secret generic openmoleuser --from-file auth
  ```
* Protect Ingress :

  In command line :          
  ```         
  echo "
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: ingress-with-auth
    annotations:
      nginx.ingress.kubernetes.io/auth-type: basic
      nginx.ingress.kubernetes.io/auth-secret: openmoleuser
      nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required - openmoleuser'
  spec:
    rules:
    - host: intern.iscpif.fr
      http:
        paths:
        - path: /
          backend:
            serviceName: openmole
            servicePort: 20000
        - path: /nginx
          backend:
            serviceName: nginx
            servicePort: 30844
  " | kubectl create -f -
  ```

  ```
  curl -v intern.iscpif.fr -u 'foo:bar'
  ```

## Reference
[Basic Auth on Kubernetes Ingress](https://imti.co/kubernetes-ingress-basic-auth/)
[Basic Authentication](https://github.com/kubernetes/ingress-nginx/tree/master/docs/examples/auth/basic)