# Kubernetes cheat sheet
An overview of the `kubectl` command for deploying OpenMole

## Deploying a containerized web application - OpenMole
```
kubectl run my-openmole --image=openmole/openmole --replicas=1 -- openmole --port 23000 --password password --remote --http
kubectl expose deployment my-openmole --port=23000 --type=LoadBalancer
kubectl get svc my-openmole
kubectl get pods
```
## Viewing resources information in cluster 
```
kubectl get po,deploy,svc,ing --all-namespaces    
kubectl get po,deploy,svc,ing --all-namespaces -o wide
```

## Viewing api-resources
```
kubectl api-resources -o wide
```

## Scaling the deployment
```
kubectl get deployments
kubectl scale deployments/my-openmole --replicas=4
kubectl get deployments
kubectl get pods -o wide
```

## Deleting the deployment
```
kubectl delete deployment my-openmole
kubectl delete svc my-openmole
```

## Editing the deployment
```
kubectl edit deployment my-openmole
```

## Entering in a pod :
```
kubectl get pods
kubectl exec -it <POD_NAME_HERE> -- /bin/bash
```

## Deleting a terminating pod :
```
kubectl delete pod <POD_NAME_HERE> --grace-period=0 --force --namespace default
```
 
## Adding node to existing Kubernetes cluster
* On node machine to be added :
  ```
  sudo apt-get update
  sudo apt install python-pip
  ```
 
* On ansible-client machine :

  ```
  ssh-copy-id ubuntu@IP-TO-ADD
  IP=(134.158.74.198 134.158.74.248 IP-TO-ADD)
  CONFIG_FILE=./kubespray/inventory/inventory.cfg python3 ./kubespray/contrib/inventory_builder/inventory.py ${IP[*]}
  cd kubespray
  ansible-playbook -i inventory/inventory.cfg cluster.yml -b -v --private-key=~/.ssh/id_rsa
  ```
  ps : suppose 134.158.74.198 and 134.158.74.248 above are 2 nodes existing in kubernetes cluster

## Removing a node from kubernetes
```
kubectl get nodes
kubectl drain <node-name>
kubectl drain <node-name> --ignore-daemonsets --delete-local-data
kubectl delete node <node-name>
```

## kubernetes dashboard
* Deploy the Dashboard UI :
  ```
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
  ```
* Access Dashboard using the kubectl command-line tool by running the following command : 
  ```
  kubectl proxy
  ```
  Kubectl will make Dashboard available at http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/.
  ![](./figs/dashboard.png)
  
* Login in Dashboard :
  * Create service account: 
    ```
    cat <<EOF | kubectl create -f -
    apiVersion: v1
    kind: ServiceAccount
    metadata:
          name: admin-user
          namespace: kube-system
    EOF
    ```
  * Create ClusterRoleBinding    
    ```
    cat <<EOF | kubectl create -f -
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: admin-user
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: cluster-admin
    subjects:
    - kind: ServiceAccount
      name: admin-user
      namespace: kube-system
    EOF
    ```

  * Get the Bearer Token. Once you run the following command, copy the token value which you will use on the following step
    ```
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
    ```
### Deploy OpenMole in kubernetes dashboard
  * In the dashboard click on `CREATE` -> `CREATE FROM FILE`, choose the files in order : `openmole-namespace.yaml` -> `openmole-deploy.yaml` -> `openmole-srv.yaml` -> `openmole-ing.yaml`
  * **pb of taint and toleration can be appeared, to solve it** : 
    * In master,
      ```
      kubectl describe node intern   # Taints : < none >
      kubectl taint node intern my-taint=test:NoSchedule
      kubectl describe node intern   # Taints:  my-taint=test:NoSchedule
      ```
    * In dashbord, find the pod **my-openmole-5cf8d68f4-jgl6f**, choose `View/edit YAML`, add in the podSpec in the part `tolerations`:
      ```
      {
           "key": "my-taint",
           "operator": "Equal",
           "value": "test",
           "effect": "NoSchedule"
      }
      ```
   
    * Use `kubectl taint` to remove taints. 
      For example, the following command removes from node `intern` all the taints with key `node-role.kubernetes.io/master`:
      ```
      kubectl taint nodes intern node-role.kubernetes.io/master-
      ```
      
## Reference
[Kubernetes Dashboard. Installation Deep Dive](http://www.joseluisgomez.com/containers/kubernetes-dashboard/)