# kubernetes dashboard
* Deploy the Dashboard UI :
  ```
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
  ```
* Access Dashboard using the kubectl command-line tool by running the following command : 
  ```
  kubectl proxy
  ```
  Kubectl will make Dashboard available at http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/.
  ![](./figs/dashboard.png)
  
* Login in Dashboard :
  * Create service account: 
    ```
    cat <<EOF | kubectl create -f -
    apiVersion: v1
    kind: ServiceAccount
    metadata:
          name: admin-user
          namespace: kube-system
    EOF
    ```
  * Create ClusterRoleBinding    
    ```
    cat <<EOF | kubectl create -f -
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: admin-user
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: cluster-admin
    subjects:
    - kind: ServiceAccount
      name: admin-user
      namespace: kube-system
    EOF
    ```

  * Get the Bearer Token. Once you run the following command, copy the token value which you will use on the following step
    ```
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
    ```
* [Deploy OpenMole in kubernetes dashboard](/kubernetes/kubernetes handbook/README.md#deploy-openmole-in-kubernetes-dashboard)