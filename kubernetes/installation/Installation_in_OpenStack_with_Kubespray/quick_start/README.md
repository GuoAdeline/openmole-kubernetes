# Deploy Kubernetes in openstack cluster

## Install python-pip in the node machines at first 
```
sudo apt-get update
sudo apt install python-pip
```

## Run the shell `kube.sh` in ansible-client machine : 
```
chmod +x kube.sh
./kube.sh
```