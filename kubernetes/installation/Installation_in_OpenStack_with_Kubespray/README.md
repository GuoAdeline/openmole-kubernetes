# Install Kubernetes cluster hosted in OpenStack with Kubespay

## Introduction - Custom Cloud Solutions - Openstack
### [Installing Kubernetes On-premises / Cloud Providers with Kubespay](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/)

This quick start helps to install a Kubernetes cluseter hosted in OpenStack with Kubespray.

Kubespray is a composition of Ansible playbooks, inventory, provisioning tools, and domain knowledge for generic OS/Kuvbernetes cluster configuration management tasks. Kubespray provides :
  * a highly available cluster
  * composable attributes
  * support for most popular Linux distributions
  * continuous integration test

#### Creating a cluster
1. Meet the underlay requirements
   * Ansible v2.5 (or newer) and python-netaddr is installed on the machine that will run Ansible commands
   * Jinja 2.9 (or newer) is required to run the Ansible Playbooks
   * The target servers must have access to the Internet in order to pull docker images
   * Your ssh key must be copied to all the servers part of your inventory
   * Terraform scripts for the cloud providers : OpenStack
2. Compose an inventory file
3. Plan your cluster deployment
4. Deploy a Cluster
5. Verify the deployment

#### Cluster operations
* Scale your cluster
* Update your cluster

## [Deploy Kubernetes clusters with Kubespray](https://juejin.im/entry/5b9e6d5a5188255c80665415)

|Host planning  |                |        | 
|:-------------:|:--------------:|:------:|
| IP            | Role           | Machine|
| 134.158.75.65 | ansible-client | kube-1 |
| 134.158.75.37 | master,node    | kube-2 |
| 134.158.75.57 | node           | kube-3 |
 
### Preparation:
* Network Configuration：
  * On the master host：(kube-2)-root
  ```
  modprobe br_netfilter
  echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
  sysctl -w net.ipv4.ip_forward=1
  ```
  
  * On the node machine：(kube-3)-root
  ```
  echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
  sysctl -w net.ipv4.ip_forward=1
  ```
  
* Configure secret login on the ansible-client machine
  * Generate ssh public and private keys
  
    Executed on ansible-cilent machine：`ssh-keygen`. Then enter three times to generate the ssh public and private keys.
  * Establish ssh unidirectional channel
   
   kube-1 : `cat /home/debian/.ssh/id_rsa.pub`
   
   It will display ssh of kube-1, copy the displayed ssh key
   
   kube-2 ： `vi .ssh`
   Paste the copied kube-1 ssh key here
   
   kube-3 : the same with kube-2
   
### Installation
1.install pip on 3 machines: 
```
sudo su
apt install python-pip
```

1.2 [Install python3](http://milq.github.io/install-python-scientific-computing-ubuntu-debian/)
 ```
 sudo apt-get update
 sudo apt-get install python3 python3-pip 
 ```

2.Install ansible :
```
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible 
sudo apt-get update
sudo apt-get install ansible python-netaddr python-pbr
```

2.2 Install Terraform
```
sudo apt-get install unzip
wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip
unzip terraform_0.11.13_linux_amd64.zip
sudo mv terraform /usr/local/bin/
terraform --version 
```

3.Update jinja : `pip install jinja2` / `pip2 install jinja2 --upgrade`

4.Install KubeSpray on an Ansible cluster

  * Download the KubeSpray code on the ansible machine and unzip it. Execute the following command：
    ```
    wget https://github.com/kubernetes-incubator/kubespray/archive/v2.1.2.tar.gz
    tar -zxvf v2.1.2.tar.gz
    mv kubespray-2.1.2 kubespray
    ```
  * Install the package required to install KubeSpray : (root) 
    ```
    cd kubespray
    pip install -r requirements.txt
    ```
  * Define the cluster : 
    ```
    IP=(134.158.75.37 134.158.75.57)
    CONFIG_FILE=./kubespray/inventory/inventory.cfg python36 ./kubespray/contrib/inventory_builder/inventory.py ${IP[*]}
    ```
  
  * View node information：
    ```
    vim ~./kubespray/inventory/inventory.cfg
    ```

5. Deploy Kubernetes clusters with ansible playbook : (not root)
  ```
  cd kubespray
  ansible-playbook -i inventory/inventory.cfg cluster.yml -b -v --private-key=~/.ssh/id_rsa
  ```

After about 10 minutes, if it goes well, the cluster will be built successfully.

## Reference
[Deploying a compute cluster in OpenStack via Terraform](https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html)

[Installing Kubernetes with Kubespray](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/)

[使用Kubespray部署生产可用的Kubernetes集群](http://www.itmuch.com/docker/kubernetes-deploy-by-kubespray/)

[使用Kubespray部署Kubernetes集群](https://juejin.im/entry/5b9e6d5a5188255c80665415)

[Install Python for scientific computing on Ubuntu or Debian](http://milq.github.io/install-python-scientific-computing-ubuntu-debian/)