# kubernetes Installation

## Installation 
* [Install kubectl binary using native package management](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-using-native-package-management)

  ```
  sudo apt-get update && sudo apt-get install -y apt-transport-https
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
  sudo apt-get update
  sudo apt-get install -y kubectl
  ```

* [Get Docker CE for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)

  * Prerequisites :
    * OS requirements:    
      * Buster 10
      * Stretch 9 (stable) / Raspbian Stretch 
    * Uninstall old versions:
      *  Older versions of Docker were called docker, docker.io , or docker-engine. If these are installed, uninstall them: 
      ```
      sudo apt-get remove docker docker-engine docker.io containerd runc
      ```

  * Install Docker CE
   * Set up the repository: 
     1. Update the apt package index: `sudo apt-get update`
     2. Install packages to allow `apt` to use a repository over HTTPS: 
        ```
        sudo apt-get install \
      		apt-transport-https \
      		ca-certificates \
      		curl \
    		gnupg2 \
    		software-properties-common
        ```
        
     3. Add Docker’s official GPG key: `curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -`. Verify that you now have the key with the fingerprint` 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`, by searching for the last 8 characters of the fingerprint: `sudo apt-key fingerprint 0EBFCD88`
     4. Use the following command to set up the stable repository. To add the nightly or test repository, add the word nightly or test (or both) after the word stable in the commands below.
        ```
        sudo add-apt-repository \
   		    "deb [arch=amd64] https://download.docker.com/linux/debian \
   		    $(lsb_release -cs) \
   		    stable"
        ```
    
   * Install Docker CE: 
     1. Update the apt package index: `sudo apt-get update`
     2. Install the latest version of Docker CE and containerd, or go to the next step to install a specific version: `sudo apt-get install docker-ce docker-ce-cli containerd.io`
     3. To install a specific version of Docker CE, list the available versions in the repo, then select and install:
       * List the versions available in your repo: `apt-cache madison docker-ce`
       * Install a specific version using the version string from the second column, for example `5:18.09.5~3-0~debian-stretch` : 
         ```
         sudo apt-get install docker-ce=5:18.09.5~3-0~debian-stretch docker-ce-cli=5:18.09.5~3-0~debian-stretch containerd.io
         ```
	  4. Verify that Docker CE is installed correctly by running the hello-world image. `sudo docker run hello-world`. 
	  
      This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.
     Docker CE is installed and running. The docker group is created but no users are added to it. You need to use `sudo` to run Docker commands. 

* [Installation of Kubernetes](https://cyril.deguet.com/fr/2018/02/12/kubernetes-debian-vps/)
  * Disable swap : `sudo swapoff -a`  
  * Install kubeadm on each VM: 
    ```
    sudo add-apt-repository "deb http://apt.kubernetes.io/etes.io/ kubernetes-xenial main"
    sudo apt-get update
    sudo apt-get install -y kubelet kubeadm kubectl
    ```

  * Run `kubeadm init` to initialize master, choosing the network address to use for pods: 
    ```
    sudo kubeadm init --pod-network-cidr=10.244.0.0/16
    ```
    
  * If all goes well, you should see the following message:
    ```
    You can now join any number of machines by running the following on each node as root:
    kubeadm join --token 184...
    ```
    
    **Note** : this `kubadm join...`command will later be used to add the worker nodes to the cluster.
    
  * To use kubectl, copy the Kubernetes configuration to the HOME of your user account (not root):
    ```
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```
  * Check if node is ready : 
    ```
    kubectl get nodes
    ```
### Problem you might meet
**Problem 1 : master node is in notReady status : [runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized]**

Check information about node :
```
kubectl describe nodes
```
In the console you will see the description of the node, run :
```
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
```

**Problem 2 : After reboot, "The connection to the server <host>:6443 was refused - did you specify the right host or port?"**
```
sudo swapoff -a
sudo systemctl daemon-reload
sudo systemctl restart kubelet
```
## Uninstallation
```
sudo kubeadm reset
sudo apt-get purge kubeadm kubectl kubelet kubernetes-cni kube*   
sudo apt-get autoremove  
sudo rm -rf ~/.kube
sudo rm -rf /etc/kubernetes/
```
## Reference
[Install and Set Up kubectl - Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

[Get Docker Engine - Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)

[Installer un cluster Kubernetes sous Debian 9](https://cyril.deguet.com/fr/2018/02/12/kubernetes-debian-vps/)