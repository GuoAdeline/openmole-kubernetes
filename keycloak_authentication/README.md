# Authentication Kubernetes - Keycloak
* [Authentication Introduction](#authentication-introduction)
* [OpenID Connect Introduction](#openid-connect)
* [Certification process that Kubernetes uses the OIDC Token](#certification-process-that-kubernetes-uses-the-oidc-token)
* [A Keycloak-based verification system](#a-keycloak-based-verification-system)
  * [Step 1. Install and run Keycloak](#step-1-install-and-run-keycloak)
  * [Step 2. Create a private CA and Certificate](#step-2-create-a-private-ca-and-certificate)
  * [Step 3. Create keystore](#step-3-create-keystore)
  * [Step 4. Configure Keycloak](#step-4-configure-keycloak)
* [User and client management in Keycloak](#user-and-client-management-in-keycloak) 
  * [Step 1. Register Client](#step-1-register-client)
  * [Step 2. Create a user](#step-2-create-a-user)
  * [Step 3. Configure kubernetes API Server](#step-3-configure-kubernetes-api-server)
  * [Step 4. Use tokens in kubectl to access the Kubernetes API Server](#step-4-use-tokens-in-kubectl-to-access-the-kubernetes-api-server)
  * [Step 5. Authorize new users](#step-5-authorize-new-users)
* [Annexe](#annexe)
  * [Use IP Address](#use-ip-address)
  * [Authentication process](#authentication-process)
* [Reference](#reference)

## Authentication introduction
Kubernetes itself provides almost no user identity management and authentication. In addition to the Service Account, all user information is stored by an external user management system, using the corresponding authentication plug-in for authentication.

The access control of the Kubernetes cluster is handled by the kube-apiserver. The access control of the kube-apiserver consists of three steps: authentication, authorization, and admission control. These three steps are performed in order. 

An identity in k8s is a set of APIs. Every API request is unique and distinct, and it must contain everything k8s needs to authenticate and authorize the request. 
K8S doesn't authenticate users, it validates assertions.

* Service Accounts

  They represent anything that isn't a person

* User Accounts

* OpenID Connect

   This is the option you should be using (with the exception of a cloud provider-based solution for a managed distribution) to authenticate users.

* How Does Kubernetes Work with OpenID Connect?

  ![](./figs/k8s_oidc_login.svg)
    1. Login to your identity provider
    2. Your identity provider will provide you with an access_token, id_token and a refresh_token
    3. When using kubectl, use your id_token with the --token flag or add it directly to your kubeconfig
    4. kubectl sends your id_token in a header called Authorization to the API server
    5. The API server will make sure the JWT signature is valid by checking against the certificate named in the configuration
    6. Check to make sure the id_token hasn’t expired
    7. Make sure the user is authorized
    8. Once authorized the API server returns a response to kubectl
    9. kubectl provides feedback to the user

  An id_token is a JSON Web Token (JWT) that says:
    1. Who the user is.
    2. What groups the user is a member of (optionally).
    3. How long the token is valid.
    4. And, it contains a digital signature to validate that the JWT hasn't been tampered with.
  
* Which Identity Provider Should I Use?
   
  When choosing an identity provider, k8s really has only two requirements:
    1. It must support OpenID Connect discovery.
    2. It provides a mechanism to generate tokens and inject them into your ~/.kube/config.

## OpenID Connect
Identifying a user through a Bear Token is a relatively secure authentication policy that is widely supported by various clients. A bearer token represents the right to access a certain resource in a certain identity. No matter who it is, any visitor who gets the token is considered to have the appropriate identity and access rights. An identity token (ID Token) is a form of bearer token that itself records the authentication statement of an authoritative certification authority on the identity of the user. It also contains a statement of which permissions are granted to the user, and Kubernetes accepts and identifies it. 

From the above, if you want ID Token, you must first go through an identity authentication process of an authoritative organization. OpenID Connect is called OIDC. It is such a set of authentication and ID Token agreement. OIDC is an extension of the [OAuth2](#oauth) protocol and is currently widely supported and used in cloud services from major brands.

OIDC's main extension to the OAuth2 protocol is that after the authentication is successful, the Auth Server can grant the ID Token in addition to the Access Token. The OIDC ID Token is a JSON Web Token (JWT) that contains predefined fields such as user UUID, user group, etc... This information is very important because Kubernetes uses this information as a User Account Profile for subsequent authorization operations.

#### OAuth
OAuth is an open network standard for authorization, which is widely used around the world. The current version is version 2.0. Simply put, OAuth is an authorization mechanism. The owner of the data tells the system to agree to authorize third-party applications to enter the system and obtain the data. The system thus generates a short-term entry token that is used in place of the password for use by third-party applications.
OAuth 2.0 specifies four processes for getting tokens:
* authorization-code : The third-party application first applies for an authorization code and then uses the code to obtain the token.
* implicit : Allow tokens to be issued directly to the front end. There is no intermediate step in the authorization code in this way t, so it is called (authorization code) "implicit".
* password : If you highly trust an application, RFC 6749 also allows the user to tell the application directly the username and password. The app uses your password to apply for a token.
* client credentials : Applicable to command line applications without a front end, that is, requesting a token at the command line.

## Certification process that Kubernetes uses the OIDC Token 
Kubernetes is both a resource (resource) server and a user agent, but it does not provide the function of guiding the user to the Auth server for authentication. Instead, it requires the user to obtain the ID token first and then access the Kubernetes API server by providing ID tokens directly. Therefore, Kubernetes does not really need to interact with the authentication server in the whole process. The entire certification process is shown below:

![](./figs/OIDC.png)

* A Kubernetes Client wants to access the Kubernetes API.
* Users first authenticate themselves to an Auth Server (such as KeyCloak or Google Accounts).
* Get id_token, refresh_token.
* The user configures the token into a client application (such as kubectl or dashboard) that needs access to the Kubernetes api.
* Kubernetes Client can use tokens to access the Kubernetes API as a user.

The key to being able to authenticate the legitimacy of a token is that all JWT tokens are digitally signed by their Auth Service. We simply configure the certificate of the Auth Server we trust in the Kubernetes API Server and use it to verify if the signature in the received id_token is valid, and then you can verify the authenticity of the token. With this PKI-based authentication mechanism, Kubernetes does not need to interact with the Auth Server during the authentication process after the configuration is complete.

## [A Keycloak-based verification system](https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication/index.html)
First you need to set up an Auth Server, which is used to provide the user identity, which is id_token. In the context of Kubernetes, we also call it Identity Provider, or IdP for short. Kubernetes has three requirements for IdP:
* Support OpenID Connection Discovery. Not all IdPs are supported.
* Support for TLS communication. Kubernetes (Api Server) requires communication with IdP only to use TLS and only use encryption methods supported by Kubernetes. 
* IdP has a CA-signed certificate (even if you use a non-commercial CA - such as a private CA of your own organization, or use a self-signed certificate).

### Step 1. Install and run Keycloak
Download the latest [Keycloak binary release](https://www.keycloak.org/downloads.html) from the official site of Keycloak
```
cd Downloads/
wget https://downloads.jboss.org/keycloak/6.0.1/keycloak-6.0.1.zip
unzip keycloak-6.0.1.zip
./keycloak-6.0.1/bin/standalone.sh   //Run the script to run a single-node mode Keycloak Server 
```

After the Server is started, you can access Keycloak's management interface locally or remotely through the browser at the following address :
```
http://localhost:8080/auth    // local
http://IP_ADDRESS:8080/auth    // remote
```

![](./figs/logininit.png)

After logging in to the management interface, you need to perform a simple initialization setup, create an administrator account, and then create a test realm, named: "Kubernetes". Specific process reference document [Keycloak Overview](https://www.keycloak.org/docs/latest/getting_started/index.html).

#### 1. Creating the Admin Account
Open http://localhost:8080/auth in your web browser. The welcome page will indicate that the server is running.

Enter a username and password to create an initial admin user.

This account will be permitted to log in to the master realm’s administration console, from which you will create realms and users and register applications to be secured by Keycloak.

![](./figs/adminConsole1.png)

#### 2. Logging in to the Admin console
```
username: openmole-admin
password: openmole-admin
```

![](./figs/login.png)

Type the username and password you created on the Welcome page to open the **Keycloak Admin Console**.
    
![](./figs/adminConsole.png)
   
#### 3. Creating a Realm and User
  
* Creating a New Realm 

  1. Go to http://localhost:8080/auth/admin/ and log in to the Keycloak Admin Console using the account you created in Install and Boot.
  2. From the Master drop-down menu, click Add Realm. When you are logged in to the master realm this drop-down menu lists all existing realms.
  3. Type demo in the Name field and click Create.
  
* Creating a New User

  To create a new user in the demo realm, along with a temporary password for that new user, complete the following steps:
  
  1.  From the menu, click Users to open the user list page.
  2.  On the right side of the empty user list, click Add User to open the add user page.
  3.  Enter a name in the Username field; this is the only required field. Click Save to save the data and open the management page for the new user.
  4.  Click the Credentials tab to set a temporary password for the new user.
  5.  Type a new password and confirm it. Click Reset Password to set the user password to the new one you specified.
  
* User Account Service

  1. After creating the new user, log out of the management console by opening the user drop-down menu and selecting **Sign Out**
  2. Go to http://localhost:8080/auth/realms/demo/account and log in to the User Account Service of your demo realm with the user you just created.
  3. Type the username and password you created. You will be required to create a permanent password after you successfully log in, unless you changed the Temporary setting to Off when you created the password.
     ```
     username: openmoledemo
     password: openmoledemo
     ```
     
#### 4.  Securing a JBoss Servlet Application
* adjusting the port used
   
  To start the Keycloak server while also adjusting the port (Run the Keycloak using a different port than the WildFly, to avoid port conflicts.):
  ```
  cd Downloads/
  ./keycloak-6.0.1/bin/standalone.sh -Djboss.socket.binding.port-offset=100
  ```
       
  The value of this property is a number that will be added to the base value of every port opened by the Keycloak server.
       
  After starting Keycloak, go to http://localhost:8180/auth/admin/ to access the admin console.
  
* Installing the Client Adapter

  Download the WildFly distribution from [wildfly.org](https://wildfly.org/downloads/)
  ```
  cd Downloads/
  unzip wildfly-17.0.0.Final.zip
  ```
  Download the WildFly OpenID Connect adapter distribution from [keycloak.org](https://www.keycloak.org/downloads.html). Extract the contents of this file into the root directory of your WildFly distribution.
  ```
  unzip keycloak-wildfly-adapter-dist-6.0.1.zip -d wildfly-17.0.0.Final/
  ```
  Run the appropriate script for your platform:
  ```
  cd Downloads/wildfly-17.0.0.Final/bin/
  ./jboss-cli.sh --file=adapter-elytron-install-offline.cli
  ```
  Start the application server.
  ```
  cd Downloads/wildfly-17.0.0.Final/bin/
  ./standalone.sh
  ```
* Downloading, Building, and Deploying Application Code
    
  You must have the following installed on your machine and available in your PATH before you continue:

  * Java JDK 8
  * Apache Maven 3.1.1 or higher from [maven.apache.org](https://maven.apache.org/download.cgi)
    ```
    cd Downloads/
    unzip apache-maven-3.6.1-bin.zip
    vim ~/.bashrc   
    export PATH="$PATH:~/Downloads/apache-maven-3.6.1/bin/"   // Add the bin directory of the created directory apache-maven-3.6.1 to the PATH environment variable
    source ~/.bashrc
    mvn -v    // Confirm with mvn -v in a new shell
  	```
  * Git
       
  Make sure your WildFly application server is started before you continue.
       
  Clone Project:
  ```
  cd Documents/stage_ISC
  git clone https://github.com/keycloak/keycloak-quickstarts
  ```
  
* Creating and Registering the Client [Configure Client Adapter Subsystem](https://github.com/keycloak/keycloak-quickstarts/blob/latest/app-profile-jee-vanilla/README.md#configure-client-adapter-subsystem) : Before configuring the adapter subsystem you need to create a client in Keycloak.
  * Open the [Keycloak admin console](http://localhost:8180/auth/admin/) with your admin account ( openmole-admin / openmole-admin )
  * In the top left drop-down menu select and manage the `Demo` realm.. Select `Clients` from the menu
  * Click `Create`
  * Add the following values:
    * Client ID: You choose (for example app-profile-vanilla)
    * Client Protocol: `openid-connect`
    * Root URL: URL to the application (for example http://localhost:8080/vanilla)
  * Click `Save` to create the client application entry.
  
    Once saved you need to change the Access Type to `confidential` and click `Save`.

* Configuring the Subsystem
       
  Next, configure the OIDC adapter via the Keycloak client adapter subsystem. To do this use the following steps:
  
  * Click on `Installation` in the tab for the client you created
  * Select `Keycloak OIDC JBoss Subsystem XML`
  * Copy the XML snippet to the clipboard
  * Open `WILDFLY_HOME/standalone/configuration/standalone.xml` in an editor
  * Locate the element `<subsystem xmlns="urn:jboss:domain:keycloak:1.1"/>` changing the XML entry from self-closing to using a pair of opening and closing tags:
    ```
    <subsystem xmlns="urn:jboss:domain:keycloak:1.1">
    </subsystem>
    ```
  * and add the above snippet within the `<subsystem>` element, as shown in this example: (Replace `WAR MODULE NAME.war` with `vanilla.war`)
    ```
    <subsystem xmlns="urn:jboss:domain:keycloak:1.1">
        <secure-deployment name="vanilla.war">
        <realm>demo</realm>
        <auth-server-url>http://localhost:8180/auth</auth-server-url>
        <ssl-required>EXTERNAL</ssl-required>
        <resource>app-profile-vanilla</resource>
        <credential name="secret">1ebd5d76-e8de-4738-9805-0692cb731ba2</credential>
        </secure-deployment>
    </subsystem>
    ```
    
Now restart the WildFly server :
```
cd Downloads/wildfly-17.0.0.Final/bin/
./standalone.sh
```

After the server is restarted :
```
cd Documents/stage_ISC/keycloak-quickstarts/app-profile-jee-vanilla
mvn clean wildfly:deploy
```

To confirm that the application is successfully deployed, go to http://localhost:8080/vanilla and a login page should appear. This time you will be redirected to Keycloak to authenticate. When the Keycloak login page opens, log in using the user you created in [Creating a New User](#3-creating-a-realm-and-user)
  
In the initial state, access to the Keycloak service uses a non-secure protocol (http), but as mentioned earlier, Kubernetes requires communication with Auth Server to use TLS, so we need the Keycloak configuration page shown in figure below. , set the "Require SSL" option to "external requests". In this way, all external requests (not requests from this Keycloak server) are forced to communicate using an SSL connection. So, the next thing to do is to set the Key and Certificate required for SSL for the server.

![](./figs/realmKube.png)

### Step 2. Create a private CA and Certificate
The key to start Keycloak SSL/HTTPS communication is to set the correct Key and Certificate. It is believed that many people will use a non-commercial CA-signed Certificate or a Self-signed Certificate in the test environment or in the internal use environment. The way we choose to use it is to create your own private CA and use it to issue your own certificate. The whole process can be done automatically by the script below, which is derived from makessl.sh.
```
cd Documents/stage_ISC/
mkdir authentication
cd authentication/
vim makessl.sh
```
paste the following :
```
#!/bin/bash

mkdir -p ssl

cat << EOF > ssl/ca.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
 
[req_distinguished_name]
 
[ v3_req ]
basicConstraints = CA:TRUE
EOF


cat << EOF > ssl/req.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
 
[req_distinguished_name]
 
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
 
[alt_names]
IP.1 = 192.168.1.136
EOF

openssl genrsa -out ssl/ca-key.pem 2048
openssl req -x509 -new -nodes -key ssl/ca-key.pem -days 365 -out ssl/ca.pem -subj "/CN=keycloak-ca" -extensions v3_req -config ssl/ca.cnf

openssl genrsa -out ssl/keycloak.pem 2048
openssl req -new -key ssl/keycloak.pem -out ssl/keycloak-csr.pem -subj "/CN=keycloak" -config ssl/req.cnf
openssl x509 -req -in ssl/keycloak-csr.pem -CA ssl/ca.pem -CAkey ssl/ca-key.pem -CAcreateserial -out ssl/keycloak.crt -days 365 -extensions v3_req -extfile ssl/req.cnf
```
then in terminal :
```
chmod +x makessl.sh
./makessl.sh
```

### Step 3. Create keystore
Keycloak is a Java based project that accepts only key pairs in the Java keystore (jks) format. We can use the keytool in the JDK to generate the keystore accepted by Keycloak and import the Private Key and Certificate we generated earlier. One thing to note here is that JDK keytool does not support direct import of keypair. We need to use PKCS12 format as a conversion intermediary. The script that generates the keystore is as follows:
```
cd ssl/
vim keystore.sh
```
paste the following in `keystore.sh` :
```
openssl pkcs12 -export -out keycloak.p12 -inkey keycloak.pem -in keycloak.crt -certfile ca.pem
keytool -importkeystore -deststorepass 'passw0rd' -destkeystore keycloak.jks -srckeystore keycloak.p12 -srcstoretype PKCS12
```
then in the terminal :
```
chmod +x keystore.sh
./keystore.sh   // passw0rd   attention!! '0' is a number here
```
```
cp keycloak.jks /home/mengxue/Downloads/keycloak-6.0.1/standalone/configuration/
```
In this way, we import the previously generated key of Private Key and Certificate, together with the CA on the entire authentication chain, into the keystore. You can check whether the newly generated keystore 'keycloak.jks' contains our Keypair.
```
keytool -list -keystore keycloak.jks -v     // passw0rd
```

### Step 4. Configure Keycloak
```
 cd /Downloads/keycloak-6.0.1/standalone/configuration/
 vim standalone.xml
```
change the following party : 
```
<security-realms>
            <security-realm name="ManagementRealm">
                <authentication>
                    <local default-user="$local" skip-group-loading="true"/>
                    <properties path="mgmt-users.properties" relative-to="jboss.server.config.dir"/>
                </authentication>
                <authorization map-groups-to-roles="false">
                    <properties path="mgmt-groups.properties" relative-to="jboss.server.config.dir"/>
                </authorization>
            </security-realm>
            <security-realm name="ApplicationRealm">
                <server-identities>
                    <ssl>
                        <keystore path="application.keystore" relative-to="jboss.server.config.dir" keystore-password="password" alias="server" key-password="password" generate-self-signed-certificate-host="localhost"/>
                    </ssl>
                </server-identities>
                <authentication>
                    <local default-user="$local" allowed-users="*" skip-group-loading="true"/>
                    <properties path="application-users.properties" relative-to="jboss.server.config.dir"/>
                </authentication>
                <authorization>
                    <properties path="application-roles.properties" relative-to="jboss.server.config.dir"/>
                </authorization>
            </security-realm>
</security-realms>
```
to : 
```
<security-realms>
            <security-realm name="ManagementRealm">
                <authentication>
                    <local default-user="$local" skip-group-loading="true"/>
                    <properties path="mgmt-users.properties" relative-to="jboss.server.config.dir"/>
                </authentication>
                <authorization map-groups-to-roles="false">
                    <properties path="mgmt-groups.properties" relative-to="jboss.server.config.dir"/>
                </authorization>
            </security-realm>
            <security-realm name="ApplicationRealm">
                <server-identities>
                    <ssl>
                        <keystore path="keycloak.jks" relative-to="jboss.server.config.dir" keystore-password="passwOrd" alias="1" key-password="passwOrd" generate-self-signed-certificate-host="localhost"/>
                    </ssl>
                </server-identities>
                <authentication>
                    <local default-user="$local" allowed-users="*" skip-group-loading="true"/>
                    <properties path="application-users.properties" relative-to="jboss.server.config.dir"/>
                </authentication>
                <authorization>
                    <properties path="application-roles.properties" relative-to="jboss.server.config.dir"/>
                </authorization>
        </security-realm>
        <security-realm name="UndertowRealm">
        <server-identities>
          <ssl> 
            <keystore path="keycloak.jks" relative-to="jboss.server.config.dir" keystore-password="passw0rd" />
          </ssl>
        </server-identities>
      </security-realm>
</security-realms>
```
Restart the Keycloak instance. Then on another test machine, use a browser to access [Keycloak admin console](https://localhost:8543/auth/admin/) 

## [Kubernetes authentication and authorization using OpenID Connect Token](https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication2/index.html)

## User and client management in Keycloak
### Step 1. Register Client
Any application that wants to use the Identity Provider (IdP) for authentication needs to register itself with the IdP to obtain a Client Id. The IdP only accepts requests from those registered clients to initiate authentication. Client Id is only required in the Kubernetes API Server, and no matter what client we use to request an Id Token from IdP, we must present a valid Client Id.

Therefore, we must first register a valid Client in Keycloak. The process is very simple, enter the "kubernetes realm" management interface we created earlier in Keycloak, select the "Clients" management interface on the left, and create a new Client through the "Create" operation, as shown in the following figure. We named the new client "kubernetes".
![](./figs/kubeClient.png)

* **Valid Redirect URL**: http://localhost:32768 . It is an important part of OIDC's security assurance. Especially for web applications, Keycloak will only return various tokens to the client and Keycloak pre-agreed URLs, preventing someone from using the client's identity to induce the client to authenticate and take tokens. For non-web applications, the OAuth2 protocol also provides another authorization mode for directly obtaining the token, which is the Password Grant Type, which uses the "token/" endpoint of the IdP, which can be provided by providing the double username and password of the User and Client. Directly obtain the token, without having to jump to the IdP's identity authentication interface to guide the client to manual authentication (username password). This approach works well for non-web applications like Kubernetes kubectl.
* **Client Secret**：The client itself also needs to be authenticated to prove its legitimacy to IdP. Keycloak supports multiple authentication forms. In the second tab page of the Client Creation interface, "Credentials", you can choose different authentication forms, as shown in the following figure. Here we choose "Client Id and Secret", which is the username and password authentication. Write down this credential and we will use it later in the process of getting the token. (7c1798f9-4daf-4cc4-97af-fcf34b10e9db)
  
  ![](./figs/credential.png)

### Step 2. Create a user
We create a test user (theone) under "Kubernetes realm". In addition to the username, all use the default properties. After that, we can find the newly created User by clicking "view all users" in the management interface "Manage -> Users", and open it to set the initial password, as shown in the figure below. (theone / theone)

![](./figs/userPass.png)

It is important to note that the user must be activated before it can be used.

The default activation method is to open the console of "kubernetes realm" https://192.168.1.136:8543/auth/admin/Kubernetes/console, and log in with the newly created user "theone" and the password we just reset. You will see the prompt to change the password again and activate the user as shown below:
![](./figs/userKube.png)

But login in is forbidden here for the moment

![](./figs/userForbidden.png)

### Step 3. Configure kubernetes API Server
| Parameter | Description | Example |
| --- | --- | --- |
| --oidc-issuer-url  | The URL provided by IdP (Keycloak in this example) to get the certificate that signed the JWT token from the IdP. This URL is the IdP Discovery URL, but removes all Path sections. | https://1.2.3.4:8443/auth/realms/kubernetes |
| --oidc-client-id  | What needs to be set here is the Client Id we created earlier in Keycloak. | kubernetes | 

 #### JWT
 Id_token is a JSON Web Token (JWT). 
 
 JWT is an open standard (RFC 7519) that defines a way to transfer JSON objects in a secure protocol between multiple points in a compact, self-consistent way. This JSON object is digitally signed and therefore verifiable and trusted. JWT can be encrypted using a symmetric encryption algorithm (algorithm HMAC) or, more commonly, using the RSA algorithm, the Public/Private key pair. The JWT consists of three parts:
  * Header — Record the encryption algorithm.
  * Payload — Records various RFC 7519-defined or user-defined fields of information called "Claim".
  * Signature — A signature for the entire token.
  
**JWT Claim related parameters in Kubernetes API Server OIDC configuration parameters** 

| Parameter | Description | Example |
| --- | --- | --- |
| --oidc-username-claim | This parameter indicates which JWT Claim is resolved by Kubernetes to User name. Use sub by default | From the id_token generated by the Keycloak decoded above, the username information is recorded in the preferred_username Claim. In this example, we select it as the value of this parameter. |
| --oidc-username-prefix | Username prefix to avoid username conflicts. We can disable Kubernetes to add any prefix by specifying the parameter "-". |  In this example, we only created one user and there is no naming conflict. We set this parameter to "-".|
| --oidc-groups-claim | Same as --oidc-username-claim, this parameter specifies which Claim is used as user group information. This parameter must be an array. | Let's set this parameter to groups first. |
| --oidc-groups-prefix | Same as --oidc-username-prefix，prefixe of Group name, but no default value | In this example, the item can be left unset. |

Modify the API Server configuration file on the Kubernetes server. The specific changes are as follows:
```
cd /etc/kubernetes/manifests
vim kube-apiserver.yaml
```
add in the kube-apiserver.yaml file :
```
- --oidc-issuer-url=https://192.168.1.136:8543/auth/realms/kubernetes
- --oidc-client-id=kubernetes
- --oidc-username-claim=preferred_username
- --oidc-username-prefix=-
- --oidc-ca-file=/etc/kubernetes/pki/ca.pem
```

https://192.168.1.136:8543/auth/realms/Kubernetes/.well-known/openid-configuration

### Step 4. Use tokens in kubectl to access the Kubernetes API Server
```
curl -k 'https://192.168.1.136:8543/auth/realms/Kubernetes/protocol/openid-connect/token' -d "client_id=kubernetes" -d "client_secret=7c1798f9-4daf-4cc4-97af-fcf34b10e9db" -d "response_type=code token" -d "grant_type=password" -d "username=theone" -d "password=theone" -d "scope=openid" | jq '.' 
```
the output is like :
```
{
"access_token":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYNThBTGR3R2JGNWhicGJGR2JYRmYycXBmOU83Y29MQVJJYV9lUlBYTEJBIn0.eyJqdGkiOiIwMDk1M2M5MS1iYzk1LTQ1NDUtYmRkOC04YWRhNzc0Zjc3YTciLCJleHAiOjE1NjIyMzA5ODksIm5iZiI6MCwiaWF0IjoxNTYyMjMwNjg5LCJpc3MiOiJodHRwczovLzE5Mi4xNjguMS4xMzY6ODU0My9hdXRoL3JlYWxtcy9LdWJlcm5ldGVzIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjAwMWMzZTQxLWJiYTctNGU4ZC1hOTY2LTE4N2VjYmFjNGNiYiIsInR5cCI6IkJlYXJlciIsImF6cCI6Imt1YmVybmV0ZXMiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIzZTNhZWRhZC0xN2Y3LTQyODQtYjBiZi1mNmY5NTQ3ZmJlMGUiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJncm91cHMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ0aGVvbmUifQ.rPKUfjXe3zSPVu--7SkyEN_8lp5nvpVOKp_5MrwLdvtE3me8Iv2KuKKOBigAS5MXjSIcQTJIw7MsdU6H6-d12xJCbnZ6f-bnQYbijB_ghKqoVnYPSKMI6QUHMZnHxOLXZv3N9H4vIFBWqfUJzIHiBfqK-MRkSmnUibSCJPgM-WopsLFfYPqYVTeS0_wR5VHZ8IUAozNqKcBUoKb7PGEupl0f8DxzcWSuIZnYUaKNf_wQ78tfTjc6r7mCju4hNK0vg-b9UgCJTWOAfOJFQ9YEoct8G9hNtq7wN7JCVhfFWJm2RBXhVWK9n9VcNqrb6SmHcHDr5I70qugsgCD13eYanA",
"expires_in":300,
"refresh_expires_in":1800,
"refresh_token":"eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjMjQwOWNhZC1kZGM5LTQ2YTAtOWIxMC0zOGQ0ZWQwYzJhZWUifQ.eyJqdGkiOiI4MTY1YjZlZC0zNWRkLTRkNDEtYjgzNC03ZGQ0YzFhYjYyYTgiLCJleHAiOjE1NjIyMzI0ODksIm5iZiI6MCwiaWF0IjoxNTYyMjMwNjg5LCJpc3MiOiJodHRwczovLzE5Mi4xNjguMS4xMzY6ODU0My9hdXRoL3JlYWxtcy9LdWJlcm5ldGVzIiwiYXVkIjoiaHR0cHM6Ly8xOTIuMTY4LjEuMTM2Ojg1NDMvYXV0aC9yZWFsbXMvS3ViZXJuZXRlcyIsInN1YiI6IjAwMWMzZTQxLWJiYTctNGU4ZC1hOTY2LTE4N2VjYmFjNGNiYiIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJrdWJlcm5ldGVzIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiM2UzYWVkYWQtMTdmNy00Mjg0LWIwYmYtZjZmOTU0N2ZiZTBlIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0.3K7otPiwomh6GF450h4_ooTYbtSjAC2gzPfgSBkqD4w",
"token_type":"bearer",
"id_token":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYNThBTGR3R2JGNWhicGJGR2JYRmYycXBmOU83Y29MQVJJYV9lUlBYTEJBIn0.eyJqdGkiOiI4ZDUzYmVmYS0wODVmLTQ3YjgtOTBiNS1kOTE0YWQ1ODJkNzQiLCJleHAiOjE1NjIyMzA5ODksIm5iZiI6MCwiaWF0IjoxNTYyMjMwNjg5LCJpc3MiOiJodHRwczovLzE5Mi4xNjguMS4xMzY6ODU0My9hdXRoL3JlYWxtcy9LdWJlcm5ldGVzIiwiYXVkIjoia3ViZXJuZXRlcyIsInN1YiI6IjAwMWMzZTQxLWJiYTctNGU4ZC1hOTY2LTE4N2VjYmFjNGNiYiIsInR5cCI6IklEIiwiYXpwIjoia3ViZXJuZXRlcyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjNlM2FlZGFkLTE3ZjctNDI4NC1iMGJmLWY2Zjk1NDdmYmUwZSIsImFjciI6IjEiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImdyb3VwcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl0sInByZWZlcnJlZF91c2VybmFtZSI6InRoZW9uZSJ9.aEdLVM3dz3KiFOOSGoVlZnNCNkI2KUPLwFcfnvmLaMLxczorZO0a0OuaZYIfkhmi4qgmDGwSGtHqZvztxjQ1QomCoS2yzpJg53ET0lOXcvnNuSH2bwBbiAt33toqLb6KDxykbO2VENkKjPSLtz1NR31Sd6J_3sLfALw2t4vSOAKvbKKJEbIijKmO-lgH4Ab0lc-apwSEHQKJLm9a-fTYHXhqsk-Lz5-Zm88HHjBd8dukjRL5EBNuGcLVEfmLImGId12_-EieddS_ZwFmBw-3m_yeaFEuQSUhb92noFQJ5F-SEO8ybaj6JEKgsV6XF2uVVAQAdCMydF8dfTjX9H-9VQ",
"not-before-policy":0,
"session_state":"3e3aedad-17f7-4284-b0bf-f6f9547fbe0e",
"scope":"openid profile email"}
```
copy the refresh-token and id-token anf run :
```
kubectl config set-credentials theone \
 --auth-provider=oidc \
 --auth-provider-arg=idp-issuer-url=https://192.168.1.136:8543/auth/realms/Kubernetes \
 --auth-provider-arg=client-id=kubernetes \
 --auth-provider-arg=client-secret=7c1798f9-4daf-4cc4-97af-fcf34b10e9db \
 --auth-provider-arg=refresh-token=eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJjMjQwOWNhZC1kZGM5LTQ2YTAtOWIxMC0zOGQ0ZWQwYzJhZWUifQ.eyJqdGkiOiI4MTY1YjZlZC0zNWRkLTRkNDEtYjgzNC03ZGQ0YzFhYjYyYTgiLCJleHAiOjE1NjIyMzI0ODksIm5iZiI6MCwiaWF0IjoxNTYyMjMwNjg5LCJpc3MiOiJodHRwczovLzE5Mi4xNjguMS4xMzY6ODU0My9hdXRoL3JlYWxtcy9LdWJlcm5ldGVzIiwiYXVkIjoiaHR0cHM6Ly8xOTIuMTY4LjEuMTM2Ojg1NDMvYXV0aC9yZWFsbXMvS3ViZXJuZXRlcyIsInN1YiI6IjAwMWMzZTQxLWJiYTctNGU4ZC1hOTY2LTE4N2VjYmFjNGNiYiIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJrdWJlcm5ldGVzIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiM2UzYWVkYWQtMTdmNy00Mjg0LWIwYmYtZjZmOTU0N2ZiZTBlIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0.3K7otPiwomh6GF450h4_ooTYbtSjAC2gzPfgSBkqD4w \
 --auth-provider-arg=idp-certificate-authority=/etc/kubernetes/pki/ca.pem \
 --auth-provider-arg=id-token=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYNThBTGR3R2JGNWhicGJGR2JYRmYycXBmOU83Y29MQVJJYV9lUlBYTEJBIn0.eyJqdGkiOiI4ZDUzYmVmYS0wODVmLTQ3YjgtOTBiNS1kOTE0YWQ1ODJkNzQiLCJleHAiOjE1NjIyMzA5ODksIm5iZiI6MCwiaWF0IjoxNTYyMjMwNjg5LCJpc3MiOiJodHRwczovLzE5Mi4xNjguMS4xMzY6ODU0My9hdXRoL3JlYWxtcy9LdWJlcm5ldGVzIiwiYXVkIjoia3ViZXJuZXRlcyIsInN1YiI6IjAwMWMzZTQxLWJiYTctNGU4ZC1hOTY2LTE4N2VjYmFjNGNiYiIsInR5cCI6IklEIiwiYXpwIjoia3ViZXJuZXRlcyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjNlM2FlZGFkLTE3ZjctNDI4NC1iMGJmLWY2Zjk1NDdmYmUwZSIsImFjciI6IjEiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImdyb3VwcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl0sInByZWZlcnJlZF91c2VybmFtZSI6InRoZW9uZSJ9.aEdLVM3dz3KiFOOSGoVlZnNCNkI2KUPLwFcfnvmLaMLxczorZO0a0OuaZYIfkhmi4qgmDGwSGtHqZvztxjQ1QomCoS2yzpJg53ET0lOXcvnNuSH2bwBbiAt33toqLb6KDxykbO2VENkKjPSLtz1NR31Sd6J_3sLfALw2t4vSOAKvbKKJEbIijKmO-lgH4Ab0lc-apwSEHQKJLm9a-fTYHXhqsk-Lz5-Zm88HHjBd8dukjRL5EBNuGcLVEfmLImGId12_-EieddS_ZwFmBw-3m_yeaFEuQSUhb92noFQJ5F-SEO8ybaj6JEKgsV6XF2uVVAQAdCMydF8dfTjX9H-9VQ
kubectl config set-context theone@kubernetes --cluster=kubernetes --user=theone --namespace=default
kubectl config use-context theone@kubernetes
```

### Step 5. Authorize new users
* Create ClusterRoleBinding
  Run in terminal :
  ```
  kubectl create -f - -o yaml <<EOF
  kind: ClusterRoleBinding
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    name: managers-role-binding
  subjects:
  - kind: Group
    name: manager 
    apiGroup: rbac.authorization.k8s.io
  roleRef:
    kind: ClusterRole
    name: cluster-admin
    apiGroup: rbac.authorization.k8s.io
  EOF
  ```
  
  Add grouping properties to users in Keyclock
  
  ![](./figs/usergroups.png)
  
* Create a **OIDC Token Mapping** to have this attribute included in the id_token
  ![](./figs/clientMapper.png)

  #### PS : 
  * open `kube-apiserver.yaml` file : 
    ```
    sudo su
    vim /etc/kubernetes/manifests/kube-apiserver.yaml
    ``` 
    add `--oidc-groups-claim=groups` in the file

## Annexe
##### Use IP Address 
```
cd Doenloads/
vim keycloak-6.0.1/standalone/configuration/standalone.xml
```
search this phrase : `jboss.bind.address.management` and change 127.0.0.1 with the IP address of the network card (192.168.1.136). Idem for `jboss.bind.address`
```
./keycloak-6.0.1/bin/standalone.sh -Djboss.socket.binding.port-offset=100
```
Then connect to [Keycloak admin console](http://192.168.1.136:8180/auth/admin/) 

##### Authentication process
![](./figs/authProccess.png)

    1. User hits the sign in button on the website,
    2. The website redirects them to the Identity Provider,
    3. Browser loads the Identity Provider login screen,
    4. User logs in using their username and password,
    5. Identity Provider redirects them back to the website with an authentication code in the query string,
    6. Browser loads website with the authentication code in query string,
    7. Website server exchanges the code for the ID token.

## Reference
[Kubernetes Identity Management: Authentication](https://www.linuxjournal.com/content/kubernetes-identity-management-authentication)

[为 Kubernetes 搭建支持 OpenId Connect 的身份认证系统](https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication/index.html)

[Keycloak Overview](https://www.keycloak.org/docs/latest/getting_started/index.html)

[使用 OpenID Connect Token 进行 Kubernetes 身份认证和授权](https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication2/index.html)