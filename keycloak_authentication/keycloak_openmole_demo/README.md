# Keycloak - openmole

## 1. Run `keycloak` server : 
```
git clone git@gitlab.com:GuoAdeline/keycloak-server.git
cd keycloak-server/
./bin/standalone.sh -Djboss.socket.binding.port-offset=100
```
keycloak server will run at http://localhost:8180/
  
## 2. Run React web client :
```
npm start
```
 
A welcome page will open at localhost:3000
 
![](./figs/kk_op_welcome.png)
 
Click `Try it now`, you will be redirect to keycloak login page :
 
![](./figs/kk_op_login.png)
 
Log in with user info `testuser / testuser` precreated, or you can create a new user by clcking `Register` in the login page
 
You will be redirected to openmole secured page :
 
![](./figs/kk_op_secure.png)
 
You can log out by clicking the `Logout` button
 
 
 