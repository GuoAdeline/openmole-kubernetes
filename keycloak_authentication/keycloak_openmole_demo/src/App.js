import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Welcome from './Welcome';
import Secured from './Secured';
import './App.css';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div align="center" className="container">
        <img src="logo.svg" />

          <Route exact path="/" component={Welcome} />
          <Link to="/secured">Try it now !</Link>
          <Route path="/secured" component={Secured} />


        </div>
      </BrowserRouter>
    );
  }
}
export default App;
