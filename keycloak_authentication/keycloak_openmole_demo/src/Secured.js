import React, { Component } from 'react';
import Keycloak from 'keycloak-js';
import UserInfo from './UserInfo';
import Logout from './Logout';

class Secured extends Component {

  constructor(props) {
    super(props);
    this.state = { keycloak: null, authenticated: false };
  }

  componentDidMount() {
    const keycloak = Keycloak('/keycloak.json');
    keycloak.init({onLoad: 'login-required'}).then(authenticated => {
      this.setState({ keycloak: keycloak, authenticated: authenticated })
    })
  }

  render() {
    if (this.state.keycloak) {
      if (this.state.authenticated) return (
        <div>
          <p>This is a Keycloak-secured component of openmole application. You shouldn't be able
          to see this unless you've authenticated with Keycloak.</p>
          <br /><br />
	      <UserInfo keycloak={this.state.keycloak} />
	      <br /><br />
          <a href="http://192.168.1.136:31772">Visit my openmole page</a>
          <br /><br />
          <br /><br />
          <Logout keycloak={this.state.keycloak} />


        </div>
      ); else return (<div>Unable to authenticate!</div>)
    }
    return (
      <div>Initializing Keycloak...</div>
    );
  }
}
export default Secured;
