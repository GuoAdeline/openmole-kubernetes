# Nginx & Nginx plus with Kubernetes
* [Installation](#installation)
* [Configuring NGINX Plus to Handle JWTs and Content‑Based Routing in kubernetes](#configuring-nginx-plus-to-handle-jwts-and-content‑based -ruoting-in-kubernetes)
* [Deploying NGINX Plus with Docker](#deploying-nginx-plus-with-docker)
* [Building nginx plus ingress image](#building-nginx-plus-ingress-image)

## Installation
### Nginx
```
lsb_release -a
sudo vim /etc/apt/sources.list
```
add the following in sources.list :
```
deb http://nginx.org/packages/ubuntu/ disco nginx
deb-src http://nginx.org/packages/ubuntu/ disco nginx
```
in terminal :
```
sudo apt-get update
sudo apt-get install nginx
```
### Nginx plus :
**prerequis** : Nginx installed

```
sudo cp -a /etc/nginx /etc/nginx-plus-backup
sudo cp -a /var/log/nginx /var/log/nginx-plus-backup
sudo mkdir -p /etc/ssl/nginx
```
Log in to NGINX Customer Portal and download the following two files:
  
    nginx-repo.key
    nginx-repo.crt
 
In terminal :   
```
sudo cp nginx-repo.key /etc/ssl/nginx/
sudo cp nginx-repo.crt /etc/ssl/nginx/
sudo wget http://nginx.org/keys/nginx_signing.key && sudo apt-key add nginx_signing.key
sudo apt-get install apt-transport-https lsb-release ca-certificates
printf "deb https://plus-pkgs.nginx.com/ubuntu `lsb_release -cs` nginx-plus\n" | sudo tee /etc/apt/sources.list.d/nginx-plus.list
sudo wget -P /etc/apt/apt.conf.d https://cs.nginx.com/static/files/90nginx
sudo apt-get update
sudo apt-get install nginx-plus
nginx -v
```

## Configuring NGINX Plus to Handle JWTs and Content‑Based Routing in kubernetes
### Generate jwk
It is to be used by NGINX Plus to verify JWT signatures. View [here](/exemple_openstack/README.md#generate-jwk) for a demo to generate jwk. 

### [Deploy OpenMole service](/exemple_openstack/README.md/openmole-service-management)

### Configuring NGINX Plus with JWT validation 
* Configuring JWT Validation and Content‑Based Routing

  Rename `default.conf` so that NGINX Plus doesn’t read it:
  ```
  sudo mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bak
  ```
  Edit configuration :
  ```
  sudo vim /etc/nginx/conf.d/backend.conf
  ```
  View [here](/exemple_openstack/README.md#nginx-configuration) for `backend.conf` demo.

* Logging JWT Data :
  ```
  vim /etc/nginx/nginx.conf
  ```
  ```
  log_format jwt '$remote_addr - $remote_user [$time_local] "$request" '
                 '$status $body_bytes_sent "$http_referer" "$http_user_agent" '
                 '$jwt_header_alg $jwt_claim_login $cookie_openmole_cookie';

  ```
* Update nignx

  1. Run the following command to test the complete configuration for syntactic validity :
  ```
  nginx -t
  ```
  2. Reload NGINX Plus.
  ```
  sudo nginx -s reload
  ```

View another method putting NGINX Plus in a Kubernetes pod on a node [here](/exemple_openstack/README.md#kubernetes-services-with-nginx-plus) (this method doesn't need to install nginx plus)
## Deploying NGINX Plus with Docker 
View [here](/docker/make_docker_image_for_nginx_plus) for details

## [Building NGINX plus ingress image](https://github.com/nginxinc/kubernetes-ingress/blob/master/build/README.md) :
```
git clone https://github.com/nginxinc/kubernetes-ingress/
cd kubernetes-ingress/
cp ~/Downloads/nginx-repo.crt ./
cp ~/Downloads/nginx-repo.key ./
docker login
make DOCKERFILE=DockerfileForPlus PREFIX=mm768528/nginx-plus-ingress
```

deploy nginx plus ingress controller :
```
mkdir ingress-deploy
cd ingress-deploy
git clone https://github.com/nginxinc/kubernetes-ingress.git
cd kubernetes-ingress/deployments/
```
Generate key and cert 
```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out MyCertificate.crt -keyout MyKey.key
```
copy cert and key into `common/default-server-secret.yaml`
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout default.key -out default.crt -subj "/CN=NGINXIngressController"
cat default.key default.crt > default.pem
sudo mkdir /etc/nginx/secrets/
sudo cp default.pem /etc/nginx/secrets/default
make DOCKERFILE=DockerfileForPlus PREFIX=mm768528/nginx-plus-ingress
```
deployment :
```
kubectl apply -f common/ns-and-sa.yaml
kubectl apply -f common/default-server-secret.yaml
kubectl apply -f common/nginx-config.yaml
kubectl apply -f deployment/nginx-plus-ingress.yaml
```

## Reference
[NGINX Ingress Controller](https://github.com/nginxinc/kubernetes-ingress/blob/master/build/README.md)

[Authentication and Content-Based Routing with JWTs and NGINX Plus](https://www.nginx.com/blog/authentication-content-based-routing-jwts-nginx-plus/)

[Load Balancing Kubernetes Services with NGINX Plus](https://www.nginx.com/blog/load-balancing-kubernetes-services-nginx-plus/)

[Deploying NGINX Plus with Docker](https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/)