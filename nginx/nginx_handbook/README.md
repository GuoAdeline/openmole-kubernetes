# Nginx notes
## Useful command
```
sudo nginx -s reload
sudo systemctl stop nginx
sudo systemctl start nginx
systemctl status nginx.service
```
## HttpRewrite
### break : completes the current set of rules
### if : checks the truth of a condition
* `=` and `!=` : the comparaison of variable with the line
* `~` and ` !~` : pattern mathching with regular expressions (case sensitive)
* `~*` and `!~*` : pattern matching with regular expressions
* `-f` and `!-f` : checking for the existence of a file
* `-d` and `!-d` : checking for the existence of a directory
* `-e` and `!-e ` : checking existence of a file, directory or symbolic link
* `-x` and `!-x` : checking whether a file is executable
* Parts of the regular expressions can be in parentheses, whose value can then later be accessed in the `$1 to >$9`
  
  For exemple :
  ```
  if ($http_user_agent ~ MSIE) {
    rewrite  ^(.*)$  /msie/$1  break;
  }
  ```

### return 
ex: `return 403 "the URL doesn't match either rewrite directive";`

### rewrite : rewrite regex replacement flag
Be aware that the rewrite regex only matches the relative path instead of the absolute URL. If you want to match the hostname, you should use an if condition, like so: 
```
if ($host ~* www\.(.*)) {
  set $host_without_www $1;
  rewrite ^(.*)$ http://$host_without_www$1 permanent; # $1 contains '/foo', not 'www.mydomain.com/foo'
}
```
Flags make it possible to end the execution of rewrite directives. Flags can be any of the following: 
* **last** - completes processing of rewrite directives, after which searches for corresponding URI and location 
* **break** - completes processing of rewrite directives 
* **redirect** - returns temporary redirect with code 302
* **permanent** - returns permanent redirect with code 301 

注: 对花括号( { 和 } )来说, 他们既能用在重定向的正则表达式里，也是用在配置文件里分割代码块, 为了避免冲突, 正则表达式里带花括号的话，应该用双引号（或者单引号）包围。比如，要将类似以下的url
```
/photos/123456 
```
重定向到：
 ```
/path/to/photos/12/1234/123456.png 
```
可以用以下方法 (注意双引号):
```
rewrite  "/photos/([0-9] {2})([0-9] {2})([0-9] {2})" /path/to/photos/$1/$1$2/$1$2$3.png; 
```
