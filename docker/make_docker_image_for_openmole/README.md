# Make docker image for `openmole-connect` 
## Image local :
### Get `openmole-connect` 
```
git clone https://gitlab.openmole.org/openmole/openmole-connect.git
```
### Install sbt
* Install JDK
  ``` 
  sudo su
  apt-get update && apt-get upgrade
  apt-get install default-jdk
  exit
  ```
* Install `sbt`
  ```
  echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
  sudo apt-get update
  sudo apt-get install sbt
  ```
* Install `npm`
  ``` 
  sudo apt install npm
  ```

### Make docker image
* openmole-connect :
  ```
  cd openmole-connect/
  sbt application/docker:publishLocal
  ```
  
## Image remote :
* create repository `openmole-connect` in dockerHub
* In command line :
  ```
  sudo usermod -a -G docker $USER
  sudo docker login
  docker images
  docker tag <IMAGE ID> <yourhubusername>/<IMAGE NAME>:<TAG>
  docker push <yourhubusername>/<REPOSITORY NAME>
  ```
  For exemple : 
  ```
  docker tag 619ff956bec9 mm768528/openmole-connect:latest
  docker push mm768528/openmole-connect
  ```
  
## Reference 
[SBT Native Packager](https://github.com/sbt/sbt-native-packager)