## Deploy OpenMole via docker
* openmole :
  ```
  docker run --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -p 8443:8443 openmole/openmole openmole --mem 2G --remote --http --port 8443
  ```
  then in browser : http://localhost:8443

* openmole-connect :
  ```
  docker run --name openmoleApplication -p 7171:8080 openmole-connect:latest --secret bar-auth --openmole-manager http://localhost:7170
  ```
  then in browser : http://localhost:7171
  
* openmole-manager :
  ```
  docker run --name openmoleManager -p 7170:8080 openmole-manager:latest
  ```
  then in browser : http://localhost:7170
  
## Entering in a container
```
docker exec -it openmoleApplication bash
```

## Kubernetes? Docker? What is the difference?

* Containers are a way of packaging software. 
* **Build and deploy containers with Docker** : Docker helps you create and deploy software within containers. Dockerfiles define a build process, which, when fed to the `docker build` command, will produce an immutable docker image. 
* **Manage Containers with Kubernetes** : Kubernetes is an open source container orchestration platform, allowing large numbers of containers to work together in harmony, reducing operational burden.  It helps with things like:
  * Running containers across many different machines
  * Scaling up or down by adding or removing containers when demand changes
  * Keeping storage consistent with multiple instances of an application
  * Distributing load between the containers
  * Launching new containers on different machines if something fails