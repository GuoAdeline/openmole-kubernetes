# Deploying NGINX Plus with Docker
## Prerequisite
Before you can create the NGINX Plus Docker image, you have to download your version of the `nginx-repo.crt` and `nginx-repo.key` files. 

## Create docker image
* To generate an NGINX Plus image, first create a `Dockerfile`.  
* Creating the NGINX Plus Image

  With the `Dockerfile`, `nginx-repo.crt`, and `nginx-repo.key` files in the same directory, run the following command there to create a Docker image called nginxplus
  ```
  docker build --no-cache -t nginxplus .
  ```
  To see if the image was created successfully:
  ```
  docker images nginxplus
  ```
  
* Test
  
  To create a container named mynginxplus based on this image, run this command:
  ```
  docker run --name mynginxplus -p 80:80 -d nginxplus
  ```
  
## Image remote
* create repository `nginxplus` in dockerHub
* In command line :
  ```
  sudo docker login
  docker images
  docker tag <IMAGE ID> <yourhubusername>/<IMAGE NAME>:<TAG>
  docker push <yourhubusername>/<REPOSITORY NAME>
  ```
  For exemple : 
  ```
  docker tag 63ed1d9ad9e2 mm768528/nginxplus:latest
  docker push mm768528/nginxplus
  ```
  
## Reference
[Deploying NGINX Plus with Docker](https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/)