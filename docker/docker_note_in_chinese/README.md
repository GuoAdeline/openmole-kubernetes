## Docker command
* check docker information
  ```
  docker info
  ```
* Docker里还没有镜像或是容器。所以，让我们通过使用命令预先构建的镜像来创建来一个：
   ```
   sudo docker pull busybox 
   ```
  BusyBox是一个最小的Linux系统，它提供了该系统的主要功能，不包含一些与GNU相关的功能和选项。
  
* 运行一个“Hello World”的例子，我们暂且叫它“Hello Docker”吧 : 
  ```
  docker run busybox /bin/echo Hello Docker 
  ```
* 以后台进程的方式运行hello docker：
  ```
  sample_job=$(docker run -d busybox /bin/sh -c "while true; do echo Docker; sleep 1; done")
  ```
  sample_job命令会隔一秒打印一次Docker，使用`docker logs` 可以查看输出的结果。如果没有给这个job起名字，那这个job就会被分配一个id，以后使用命令例如docker logs查看日志就会变得比较麻烦。
* 运行docker logs命令来查看job的当前状态：
  ```
  docker logs $sample_job
  ```
* 所有Docker命令可以用以下命令查看：
  ```
  docker help
  ```
* 名为sample_job的容器，可以使用以下命令来停止：
  ```
  docker stop $sample_job
  ``` 
* 名为sample_job的容器，可以使用以下命令来停止：
  ```
  docker stop $sample_job
  ```
* 使用以下命令可以重新启动该容器：
  ```
  docker restart $sample_job
  ``` 
* 如果要完全移除容器，需要先将该容器停止，然后才能移除 :
  ```
  docker stop $sample_job
  docker rm $sample_job
  ```  
* 将容器的状态保存为镜像，使用以下命令：
  ```
  docker commit $sample_job job1
  ```
  注意，镜像名称只能取字符[a-z]和数字[0-9]。
* 现在，你就可以使用以下命令查看所有镜像的列表：
  ```
  docker images
  ```
* 在我们之前的Docker教程中，我们学习过镜像是存储在Docker registry。在registry中的镜像可以使用以下命令查找到：
  ```
  docker search (image-name)
  ```
* 查看镜像的历史版本可以执行以下命令：
  ```
  docker history (image_name)
  ```
* 最后，使用以下命令将镜像推送到registry：
  ```
  docker push (image_name)
  ```
  非常重要的一点是，你必须要知道存储库不是根存储库，它应该使用此格式(user)/(repo_name)
  
* daemon： Docker daemon是一个用于管理容器的后台进程。一般情况下，守护进程是一个长期运行的用来处理请求的进程服务。`-d`参数用于运行后台进程。
* build：可以使用Dockerfile来构建镜像。简单的构建命令如下：
  ```
  docker build [options] PATH | URL
  ```
  `--rm=true`表示构建成功后，移除所有中间容器
 
  `--no-cache=false`表示在构建过程中不使用缓存
  
* attach: Docker允许使用attach命令与运行中的容器交互，并且可以随时观察容器內进程的运行状况。attach命令的语法是：
  ```
  docker attach container
  ```
* diff： Docker提供了一个非常强大的命令diff，它可以列出容器内发生变化的文件和目录。这些变化包括添加（A-add）、删除（D-delete）、修改（C-change）。语法是：
  ```
  docker diff container
  ```
* import：Docker可以导入远程文件、本地文件和目录。使用HTTP的URL从远程位置导入，而本地文件或目录的导入需要使用-参数。从远程位置导入的语法是：
  ```
  docker import http://example.com/example.tar
  ```
* export：类似于import，export命令用于将容器的系统文件打包成tar文件。
  ```
  docker ps -a
  sudo docker export CONTAINER_ID > image.tar
  ```
* cp：这个命令是从容器内复制文件到指定的路径上。语法如下：
  ```
  docker cp container:path hostpath
  ```
* inspect：Docker inspect命令可以收集有关容器和镜像的底层信息。这些信息包括：

   * 容器实例的IP地址
   * 端口绑定列表
   * 特定端口映射的搜索
   * 收集配置的详细信息
   
  该命令的语法是：
  ```
  docker inspect container/image
  ```
* kill：发送SIGKILL信号来停止容器的主进程。语法是：
  ```
  docker kill [options] container
  ```
* rmi：该命令可以移除一个或者多个镜像，语法如下：
  ```
  docker rmi image
  ```
* load：该命令从tar文件中载入镜像或仓库到STDIN; 如载入app_box.tar到STDIN：
  ```
  docker load -i app_box.tar
  ```
* save：类似于load，该命令保存镜像为tar文件并发送到STDOUT。语法如下：
  ```
  docker save IMAGE_ID > app_box.tar
  ```
  
## Dockerfile
* Dockerfile支持支持的语法命令如下：
  ```
  INSTRUCTION argument
  ```
  指令不区分大小写。但是，命名约定为全部大写。
* 所有Dockerfile都必须以`FROM`命令开始。 `FROM`命令会指定镜像基于哪个基础镜像创建，接下来的命令也会基于这个基础镜像
  ```
  FROM <image name>
  ```
* MAINTAINER：设置该镜像的作者 :
  ```
  MAINTAINER <author name>
  ```
* RUN：在shell或者exec的环境下执行的命令。RUN指令会在新创建的镜像上添加新的层面，接下来提交的结果用在Dockerfile的下一条指令中。语法如下：
  ```
  RUN 《command》
  ```
* ADD：复制文件指令。它有两个参数<source>和<destination>。destination是容器内的路径。source可以是URL或者是启动配置上下文中的一个文件。语法如下：
  ```
  ADD 《src》 《destination》
  ```
* CMD：提供了容器默认的执行命令。 Dockerfile只允许使用一次CMD指令。 使用多个CMD会抵消之前所有的指令，只有最后一个指令生效。 CMD有三种形式：
  ```
  CMD ["executable","param1","param2"]
  CMD ["param1","param2"]
  CMD command param1 param2
  ``` 
* EXPOSE：指定容器在运行时监听的端口。语法如下：
  ```
  EXPOSE <port>;
  ```
* ENTRYPOINT：配置给容器一个可执行的命令，这意味着在每次使用镜像创建容器时一个特定的应用程序可以被设置为默认程序。同时也意味着该镜像每次被调用时仅能运行指定的应用。类似于CMD，Docker只允许一个ENTRYPOINT，多个ENTRYPOINT会抵消之前所有的指令，只执行最后的ENTRYPOINT指令。语法如下：
  ```
  ENTRYPOINT ["executable", "param1","param2"]
  ENTRYPOINT command param1 param2
  `` 
* WORKDIR：指定RUN、CMD与ENTRYPOINT命令的工作目录。语法如下：
  ```
  WORKDIR /path/to/workdir
  ```
* ENV：设置环境变量。它们使用键值对，增加运行程序的灵活性。语法如下：
  ```
  ENV <key> <value>
  ```
* USER：镜像正在运行时设置一个UID。语法如下：
  ```
  USER <uid>
  ```
* VOLUME：授权访问从容器内到主机上的目录。语法如下：
  ```
  VOLUME ["/data"]
  ```  
