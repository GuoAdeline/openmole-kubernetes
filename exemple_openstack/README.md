# A demo for openmole deployment in kubernetes cluster via openstack

* [Deploy kubernetes](#deploy-kubernetes-in-openstack)
* [Deploy OpenMole Service in kubernetes](#deploy-openmole-service-in-kubernetes)
* [Kubernetes Services with NGINX Plus](#kubernetes-services-with-nginx-plus)


## [Deploy Kubernetes in openstack](kubernetes/installation/Installation_in_OpenStack_with_Kubespray/quick_start) :
* Install python-pip in the node machines at first
  ```
  sudo apt-get update
  sudo apt install python-pip
  ```
* In ansible-client machine :
  ```
  ssh ubuntu@134.158.74.248
  chmod +x kube.sh
  ./kube.sh
  cd kubespray
  ansible-playbook -i inventory/inventory.cfg cluster.yml -b -v --private-key=~/.ssh/id_rsa
  ```

## Deploy Openmole Service in kubernetes
#### Get docker images :
* login :
   ```
   sudo usermod -a -G docker $USER
   sudo docker login   // enter username and password for dockerHub
   sudo docker images
   ```
* Get images :
  ```
  docker pull mm768528/openmole-connect
  docker pull mm768528/openmole-manager
  ```
#### [Deploy `Ingress Nginx` in kubernetes cluster](/kubernetes/kubernetes_nginx_ingress/README.md) :
* ingress Nginx deployment
  ```
  kubectl apply -f ingress_nginx.yaml
  ```
* add external IP to ingress:
  ```
  kubectl patch svc <svc-name> -n <namespace> -p '{"spec": {"type": "LoadBalancer", "externalIPs":["<ADRESSE IP>"]}}'
  ```
  for exemple : 
  ```
  kubectl patch svc ingress-nginx -n ingress-nginx -p '{"spec": {"type": "LoadBalancer", "externalIPs":["134.158.75.115"]}}'
  ```
  
#### Deploy `openmole-connect` and `openmole-manager` in kubernetes cluster :
* Create secret :  
  ```
  sudo apt install apache2-utils
  htpasswd -c ./auth openmoleuser    // password : openmoleuser
  kubectl create secret generic openmoleuser --from-file auth
  ```
  
* Deployment :
  * Method 1 : using kubectl API 
    ```
    kubectl run openmole-connect --image=mm768528/openmole-connect:latest --replicas=1 --namespace=ingress-nginx -- --secret openmoleuser --openmole-manager http://skub.kb.openmole.org/manager/
    kubectl expose deployment openmole-connect --port=8080 --namespace=ingress-nginx
    kubectl run openmole-manager --image=mm768528/openmole-manager:latest --replicas=1 --namespace=ingress-nginx
    kubectl expose deployment openmole-manager --port=8080 --namespace=ingress-nginx
    ```
  * Method 2 : Using yaml file
  
    TODO
    
* Combine with ingress :
  ```
  kubectl apply -f openmole_manager_ing.yaml  
  kubectl apply -f openmole_connect_ing.yaml 
  ```
  
  Then in the browser you can find :
  * openmole-connection : http://skub.kb.openmole.org/connection
  * openmole-manager : http://skub.kb.openmole.org.manager
  
#### Deploy `openmole` in kubernetes cluster :
```
kubectl apply -f openmole.yaml  
```
Then you can get openmole at http://skub.kb.openmole.org/openmole/app

## Kubernetes Services with NGINX Plus
We are putting NGINX Plus in a Kubernetes pod on a node that we expose to the Internet. Our pod is created by a replication controller, which we are also setting up. Our Kubernetes‑specific NGINX Plus configuration file resides in a folder shared between the NGINX Plus pod and the node, which makes it simpler to maintain.

### Generate jwk
  In terminal :
  ```
  $ echo -n bar-auth | base64 | tr '+/' '-_' | tr -d '='
  YmFyLWF1dGg
  ```
  copy the output `YmFyLWF1dGg`
  ```
  cd  /etc/nginx/
  vim api_secret.jwk
  ```
  edit in the `api_secret.jwk` file :
  ```
  {"keys":
      [{
          "k":"YmFyLWF1dGg",
          "kty":"oct"
      }]
  } 
  ```

  then generate jwk where locates the `api_secret.jwk` file :
  ```
  kubectl create secret generic my-jwk --from-file=./api_secret.jwk
  ```
  It is to be used by NGINX Plus to verify JWT signatures.
  
### OpenMole service management
* Creating the Replication Controller for the Service openmole-connect and openmole-manager:
  ```
  kubectl apply -f openmole-connect-rc.yaml
  kubectl apply -f openmole-manager-rc.yaml
  ```
* Creating the Service openmole-connect and openmole-manager:
  ```
  kubectl apply -f openmole-connect-svc.yaml
  kubectl apply -f openmole-manager-svc.yaml
  ```
### Nginx configuration
* Choosing a Node to Host the NGINX Plus Pod
  ```
  kubectl get nodes
  kubectl label node kubestack-compute-1 role=nginxplus
  ```
* Configuring NGINX Plus with JWT validation 

  Create the `/etc/nginx/conf.d` folder on the node :
  ```
  sudo mkdir -p /etc/nginx/conf.d
  ```
  Then we create the `backend.conf` file there :
  ```
  vim /etc/nginx/conf.d/backend.conf
  ```
  entering these directives:
  ```
  resolver 10.233.0.3 valid=5s;   # 10.233.0.3 is cluster ip of kube-dns in kubernetes cluster

  upstream connect {
      zone upstream-connect 64k;
      server openmole-connect-svc.default.svc.cluster.local service=_http._tcp resolve;
  }

  upstream manager {
      zone upstream-manager 64k;
      server openmole-manager-svc.default.svc.cluster.local service=_http._tcp resolve;
  }

  upstream openmole {
      zone upstream-openmole 64k;
      server openmole-bar-svc.default.svc.cluster.local service=_http._tcp resolve;
  }

  server {
      server_name skub.kb.openmole.org ;
      listen 80;
  #     status_zone backend-servers;

  location /connection {
         proxy_pass http://connect;
  #       return 301 http://skub.kb.openmole.org/manager;

  }

  location /manager {

        # JWT validation
        auth_jwt "JWT Test Realm" token=$cookie_openmole_cookie;
        auth_jwt_key_file /etc/nginx/api_secret.jwk;
        error_log /var/log/nginx/host.jwt.error.log debug;

        if ( $jwt_claim_login = bar ) {
                add_header X-jwt-claim-login "$jwt_claim_login" always;
                 add_header X-jwt-status "Redirect to $jwt_claim_login" always;
                 return 302 /app;
                # proxy_pass http://manager;
          }

          if ( $jwt_claim_login != bar ) {
                  add_header X-jwt-claim-uid "$jwt_claim_login" always;
                  add_header X-jwt-status "Invalid user" always;
                 # return 301 http://skub.kb.openmole.org/connection;
                  proxy_pass http://manager;
          }


        #proxy_pass http://openmole-manager;
        #health_check;
    }
  
  location /app {
        # proxy_pass http://openmole;


         # JWT validation
         auth_jwt "JWT Test Realm" token=$cookie_openmole_cookie;
         auth_jwt_key_file /etc/nginx/api_secret.jwk;
         error_log /var/log/nginx/host.jwt.error.log debug;

        proxy_pass http://openmole;

        if ($http_cookie !~* 'openmole_cookie=([^;]+)(?:;|$)') {
               return 302 /connection;
        }
       # return 301 http://skub.kb.openmole.org/manager;

    }

  }

  server {
    server_name skub.kb.openmole.org ;
    listen 8081;
    root /usr/share/nginx/html;

    location = /dashboard.html { }

    location = / {
        return 302 /dashboard.html;
    }

    location /api {
        api write=on;
    }
  }
  ```

* Logging JWT Data :

  Add the following `log_format` directive to `/etc/nginx/nginx.conf`:
  ```
  log_format jwt '$remote_addr - $remote_user [$time_local] "$request" '
                 '$status $body_bytes_sent "$http_referer" "$http_user_agent" '
                 '$jwt_header_alg $jwt_claim_login $cookie_openmole_cookie';

  ```
### Configuring the NGINX Plus Pod 

* Configuring the Replication Controller for the NGINX Plus Pod

  * In terminal pull nginxplus docker image:
    ```
    docker login
    docker pull mm768528/nginxplus:latest
    ```
    View [here](/docker/make_docker_image_for_nginx_plus) to see how to build NGINX Plus image.
    
  * Creating the Replication Controller
    ```
    kubectl create -f nginxplus-replication-controller.yaml
    ```
* To verify the NGINX Plus pod was created, we run:
  ```
  kubectl get pods
  ```
  We can check that our NGINX Plus pod is up and running by looking at the NGINX Plus live activity monitoring dashboard, which is available on port 8081 at the external IP address of the node (so http://134.158.75.115:8081/dashboard.html or http://skub.kb.openmole.org:8081/dashboard.html in our case)
 
  ![](./figs/nginx1.png)
  
  We can also see the backends we defined in backend.conf file :
  
  ![](./figs/nginx2.png)
  
  
ps : **Deploy openmole-conenct and openmole-manager without redirect** :
  ```
  kubectl run openmole-connect --image=mm768528/openmole-connect:sansredirect --replicas=1 -- --secret bar-auth --openmole-manager http://skub.kb.openmole.org/manager/
  kubectl run openmole-manager --image=mm768528/openmole-manager:sansredirect --replicas=1
  kubectl expose deployment openmole-connect --port=8080
  kubectl expose deployment openmole-manager --port=8080
  ```
  

# Reference :
[NGINX Ingress Controller Installation Guide](https://kubernetes.github.io/ingress-nginx/deploy/#generic-deployment)

[default-backend.yaml](https://github.com/uswitch/ingress/blob/master/deploy/default-backend.yaml)

[Authentication and Content-Based Routing with JWTs and NGINX Plus](https://www.nginx.com/blog/authentication-content-based-routing-jwts-nginx-plus/)

[Load Balancing Kubernetes Services with NGINX Plus](https://www.nginx.com/blog/load-balancing-kubernetes-services-nginx-plus/)
