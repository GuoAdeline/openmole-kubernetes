# Deploy OpenMole in Kubernetes cluster

## Deploy a cluster of machines in OpenStack
View [here](/terraform/Deploy_compute_cluster_in_OpenStack_via_Terraform) for details.

## Deploy Kubernetes on a cluster of machines
View [here](/kubernetes/installation/Installation_in_OpenStack_with_Kubespray) for details.

A quick start example is [here](/kubernetes/installation/Installation_in_OpenStack_with_Kubespray/quick_start)

## A demo for OpenMOLE Deployment 
View [here](/exemple_openstack) for details.