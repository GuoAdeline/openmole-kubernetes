# Webographie / webography

Kubernetes Tutorial [en ligne]
Disponible sur : https://kubernetes.io/docs/tutorials/ [consulté le 9 avril 2019]

Install and Set Up kubectl - Kubernetes [en ligne]
Disponible sur : https://kubernetes.io/docs/tasks/tools/install-kubectl/ [consulté le 12 avril 2019]

Get Docker Engine - Community for Debian [en ligne]
Disponible sur : https://docs.docker.com/install/linux/docker-ce/debian/ [consulté le 12 avril 2019]

Installer un cluster Kubernetes sous Debian 9 [en ligne]
Disponible sur : https://cyril.deguet.com/fr/2018/02/12/kubernetes-debian-vps/  [consulté le 12 avril 2019]

Docker 和 Kubernetes 从听过到略懂：给程序员的旋风教程 [en ligne] Disponible sur : https://juejin.im/post/5b62d0356fb9a04fb87767f5 [consulté le 16 avril 2019]

Deploying a compute cluster in OpenStack via Terraform
[en ligne] Disponible sur : https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html [consulté le 18 avril 2019]

Installing Kubernetes with Kubespray [en ligne] Disponible sur : https://kubernetes.io/docs/setup/production-environment/tools/kubespray/ [consulté le 19 avril 2019]

使用Kubespray部署生产可用的Kubernetes集群 [en ligne] Disponible sur : http://www.itmuch.com/docker/kubernetes-deploy-by-kubespray/ [consulté le 19 avril 2019]

使用Kubespray部署Kubernetes集群 [en ligne] Disponible sur : https://juejin.im/entry/5b9e6d5a5188255c80665415 [consulté le 19 avril 2019]

Install Python for scientific computing on Ubuntu or Debian [en ligne] Disponible sur : http://milq.github.io/install-python-scientific-computing-ubuntu-debian/ [consulté le 19 avril 2019]

Introduction to Terraform [en ligne] Disponible sur : https://www.terraform.io/intro/index.html [consulté le 23 avril 2019]

Use Terraform to deploy Openstack VMs [en ligne] Disponible sur : https://www.youtube.com/watch?v=IK6lRLDyzpY [consulté le 23 avril 2019]

Deploying a compute cluster in OpenStack via Terraform [en ligne] Disponible sur : https://galaxyproject.github.io/training-material/topics/admin/tutorials/terraform/tutorial.html#managing-a-cluster-of-vms [consulté le 26 avril 2019]

Kubernetes Dashboard. Installation Deep Dive [en ligne] Disponible sur : 
http://www.joseluisgomez.com/containers/kubernetes-dashboard/ [consulté le 22 mai 2019]

NGINX Ingress Controller Installation Guide [en ligne] Disponible sur : https://kubernetes.github.io/ingress-nginx/deploy/#generic-deployment [consulté le 23 mai 2019]

default-backend.yaml [en ligne] Disponible sur : https://github.com/uswitch/ingress/blob/master/deploy/default-backend.yaml [consulté le 23 mai 2019]

Skuber [en ligne] Disponible sur : https://github.com/doriordan/skuber [consulté le 28 mai 2019] 

SBT Native Packager [en ligne] Disponible sur : https://github.com/sbt/sbt-native-packager [consulté le 16 juin 2019]

Kubernetes Identity Management: Authentication [en ligne] Disponible sur : https://www.linuxjournal.com/content/kubernetes-identity-management-authentication [consulté le 25 juin 2019]

为 Kubernetes 搭建支持 OpenId Connect 的身份认证系统 [en ligne] Disponible sur : 
https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication/index.html [consulté le 25 juin 2019]

为 Kubernetes 搭建支持 OpenId Connect 的身份认证系统  [en ligne] Disponible sur :  https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication/index.html [consulté le 26 juin]

Keycloak Overview [en ligne] Disponible sur : https://www.keycloak.org/docs/latest/getting_started/index.html [consulté le 26 juin 2019]

Kubernetes authentication and authorization using OpenID Connect Token [en ligne] Disponible sur : https://www.ibm.com/developerworks/cn/cloud/library/cl-lo-openid-connect-kubernetes-authentication2/index.html [consulté le 26 juin 2019]

Keycloak Server Installation and Configuration Guide [en ligne] Disponible sur : https://www.keycloak.org/docs/6.0/server_installation/ [consulté le 9 juillet 2019]

Keycloak Server Administration Guide [en ligne] Disponible sur : https://www.keycloak.org/docs/6.0/server_admin/ [consulté le 9 juillet 2019]

Basic Auth on Kubernetes Ingress [en ligne] Disponible sur : https://imti.co/kubernetes-ingress-basic-auth/ [consulté le 29 juillet 2019]

Basic Authentication [en ligne] Disponible sur : https://github.com/kubernetes/ingress-nginx/tree/master/docs/examples/auth/basic [consulté le 29 juillet 2019]

Authentication and Content-Based Routing with JWTs and NGINX Plus [en ligne] Disponible sur : https://www.nginx.com/blog/authentication-content-based-routing-jwts-nginx-plus/ [consulté le 26 aout 2019]

Load Balancing Kubernetes Services with NGINX Plus [en ligne] Disponible sur : https://www.nginx.com/blog/load-balancing-kubernetes-services-nginx-plus/ [consulté le 26 aout 2019]

Deploying NGINX Plus with Docker [en ligne] Disponible sur : https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/ [consulté le 28 aout 2019]






